package com.dodulz.papaantarin.papaantar.bean;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dodulz on 3/5/14 AD.
 */
@DatabaseTable(tableName = "travel")
public class Travel {

    @DatabaseField(generatedId = true)
    public int id;

    @DatabaseField
    public String name;

    @DatabaseField
    public String slug_name;

    @DatabaseField
    public String city;

    @DatabaseField
    public String harga;

    @DatabaseField
    public String type;

    @DatabaseField
    public String uri_image;

    @DatabaseField
    public String address;

    @DatabaseField
    public String state;

    @DatabaseField
    public String country;

    @DatabaseField
    public String facilities;

    @DatabaseField
    public int status_query;

    public Travel() {
    }

    public Travel(String name, String slug_name, String city, String harga, String type) {

        this.name = name;
        this.slug_name = slug_name;
        this.city = city;
        this.harga = harga;
        this.type = type;

    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFacilities() {
        return facilities;
    }

    public void setFacilities(String facilities) {
        this.facilities = facilities;
    }

    public int getStatus_query() {
        return status_query;
    }

    public void setStatus_query(int status_query) {
        this.status_query = status_query;
    }

    public String getUri_image() {
        return uri_image;
    }

    public void setUri_image(String uri_image) {
        this.uri_image = uri_image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug_name() {
        return slug_name;
    }

    public void setSlug_name(String slug_name) {
        this.slug_name = slug_name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return  name;
    }
}
