package com.dodulz.papaantarin.papaantar.util;

/**
 * Created by dodulz on 2/25/14 AD.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.dodulz.papaantarin.papaantar.bean.Airport;
import com.dodulz.papaantarin.papaantar.bean.Armada;
import com.dodulz.papaantarin.papaantar.bean.City;
import com.dodulz.papaantarin.papaantar.bean.Contact;
import com.dodulz.papaantarin.papaantar.bean.Rutes;
import com.dodulz.papaantarin.papaantar.bean.Travel;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DBHelp  extends OrmLiteSqliteOpenHelper {

    // name of the database file for your application -- change to something appropriate for your app
    private static final String DATABASE_NAME = "konaah.db";
    // any time you make changes to your database objects, you may have to increase the database version
    private static final int DATABASE_VERSION = 1;

    // the DAO object we use to access the SimpleData table
    private Dao<City, Integer> citysDao = null;
    private RuntimeExceptionDao<City, Integer>citysRuntimeDao = null;

    private Dao<Travel, Integer> travelsDao = null;
    private RuntimeExceptionDao<Travel, Integer>travelsRuntimeDao = null;

    private Dao<Contact, Integer> contactsDao = null;
    private RuntimeExceptionDao<Contact, Integer>contactsRuntimeDao = null;


    private Dao<Armada, Integer> armadasDao = null;
    private RuntimeExceptionDao<Armada, Integer>armadasRuntimeDao = null;

    private Dao<Rutes, Integer> rutessDao = null;
    private RuntimeExceptionDao<Rutes, Integer>rutesRuntimeDao = null;

    private Dao<Airport, Integer> airportsDao = null;
    private RuntimeExceptionDao<Airport, Integer>airportsRuntimeDao = null;

    public DBHelp(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            Log.i(DBHelp.class.getName(), "onCreate");
            TableUtils.createTable(connectionSource, City.class);
            TableUtils.createTable(connectionSource, Contact.class);
            TableUtils.createTable(connectionSource, Travel.class);
            TableUtils.createTable(connectionSource, Armada.class);
            TableUtils.createTable(connectionSource, Rutes.class);
            TableUtils.createTable(connectionSource, Airport.class);
        } catch (SQLException e) {
            Log.e(DBHelp.class.getName(), "Can't create database", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i2) {
        try {
            Log.i(DBHelp.class.getName(), "onUpgrade");
            TableUtils.dropTable(connectionSource, City.class, true);
            TableUtils.dropTable(connectionSource, Contact.class, true);
            TableUtils.dropTable(connectionSource, Travel.class, true);
            TableUtils.dropTable(connectionSource, Armada.class, true);
            TableUtils.dropTable(connectionSource, Rutes.class, true);
            TableUtils.dropTable(connectionSource, Airport.class, true);
            onCreate(sqLiteDatabase, connectionSource);
        } catch (SQLException e) {
            Log.e(DBHelp.class.getName(), "Can't drop databases", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        super.close();
        citysDao = null;
        contactsDao = null;
        travelsDao = null;
        armadasDao = null;
        rutessDao = null;
        airportsDao = null;
    }

    // Table Media
    public Dao<City, Integer> getCitysDao() throws SQLException {
        if (citysDao == null) {
            citysDao = getDao(City.class);
        }
        return citysDao;
    }

    public RuntimeExceptionDao<City,Integer> getCitysDataDao() {
        if (citysRuntimeDao == null) {
            citysRuntimeDao = getRuntimeExceptionDao(City.class);
        }
        return citysRuntimeDao;
    }

    public Dao<Travel, Integer> getTravelsDao() throws SQLException {
        if (travelsDao==null){
            travelsDao = getDao(Travel.class);
        }
        return travelsDao;
    }

    public RuntimeExceptionDao<Travel, Integer> getTravelsRuntimeDao() {
        if (travelsRuntimeDao==null){
            travelsRuntimeDao = getRuntimeExceptionDao(Travel.class);
        }
        return travelsRuntimeDao;
    }

    public Dao<Contact, Integer> getContactsDao() throws SQLException {
        if (contactsDao==null){
            contactsDao = getDao(Contact.class);
        }
        return contactsDao;
    }

    public RuntimeExceptionDao<Contact, Integer> getContactsRuntimeDao() {
        if (contactsRuntimeDao==null){
            contactsRuntimeDao = getRuntimeExceptionDao(Contact.class);
        }
        return contactsRuntimeDao;
    }

    public Dao<Armada, Integer> getArmadasDao() throws SQLException {
        if (armadasDao==null){
            armadasDao = getDao(Armada.class);
        }
        return armadasDao;
    }

    public RuntimeExceptionDao<Armada, Integer> getArmadasRuntimeDao() {
        if (armadasRuntimeDao==null){
            armadasRuntimeDao = getRuntimeExceptionDao(Armada.class);
        }
        return armadasRuntimeDao;
    }

    public Dao<Rutes, Integer> getRutessDao() throws SQLException {
        if (rutessDao==null){
            rutessDao = getDao(Rutes.class);
        }
        return rutessDao;
    }

    public RuntimeExceptionDao<Rutes, Integer> getRutesRuntimeDao() {
        if (rutesRuntimeDao==null){
            rutesRuntimeDao = getRuntimeExceptionDao(Rutes.class);
        }
        return rutesRuntimeDao;
    }

    public Dao<Airport, Integer> getAirportsDao() throws SQLException {
        if (airportsDao==null){
            airportsDao = getDao(Airport.class);
        }
        return airportsDao;
    }

    public RuntimeExceptionDao<Airport, Integer> getAirportsRuntimeDao() {
        if (airportsRuntimeDao==null){
            airportsRuntimeDao = getRuntimeExceptionDao(Airport.class);
        }
        return airportsRuntimeDao;
    }
}
