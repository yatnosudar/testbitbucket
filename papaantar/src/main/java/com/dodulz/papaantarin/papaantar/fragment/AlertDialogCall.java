package com.dodulz.papaantarin.papaantar.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.widget.ArrayAdapter;

import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.bean.Contact;
import com.dodulz.papaantarin.papaantar.bean.ContactRepo;

import java.util.List;

/**
 * Created by dodulz on 3/11/14 AD.
 */
public class AlertDialogCall extends AlertDialog.Builder {

    ArrayAdapter<String> arrayAdapter;

    public AlertDialogCall(final Context context, String slug_name, final int stat) {
        super(context);
        setIcon(R.drawable.ic_launcher);
        if (stat==1){
            setTitle("CALL TRAVEL");
        }else{
            setTitle("SMS TRAVEL");
        }
        arrayAdapter = new ArrayAdapter<String>(
                context,
                android.R.layout.select_dialog_singlechoice);
        telpn(context,slug_name,stat);

        setNegativeButton("cancel",new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        setAdapter(arrayAdapter, new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                if (stat==1){
                    callIntent.setData(Uri.parse("tel:"+arrayAdapter.getItem(i)));
                    context.startActivity(callIntent);
                }else{
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", arrayAdapter.getItem(i), null)));
                }

            }
        });
    }

    public void telpn(Context context,String slug_name,int stat){
        List<Contact> contacts = new ContactRepo(context).getAllWhere(slug_name,1);
        for (int i = 0; i<contacts.size();i++){

//            if (stat==1){
                if (contacts.get(i).getType_contact().equals("1")||contacts.get(i).getType_contact().equals("3")){
                    arrayAdapter.add(contacts.get(i).getNo_contact());
                }
//            }else{
//                if (contacts.get(i).getType_contact().equals("1")){
//                    arrayAdapter.add(contacts.get(i).getNo_contact());
//                }
//            }
        }
    }
}
