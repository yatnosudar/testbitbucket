package com.dodulz.papaantarin.papaantar.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.bean.Travel;
import com.dodulz.papaantarin.papaantar.util.StaticVariable;
import com.loopj.android.image.SmartImageView;

import java.util.List;

/**
 * Created by dodulz on 3/7/14 AD.
 */
public class AdapterResultSearch extends ArrayAdapter<Travel> {

    static class ViewHolder{
        public TextView name_result;
        public TextView city_result;
        public TextView type_result;
        public TextView price_result;
        public SmartImageView image_result;
    }

    public Activity context;
    public int textViewResourceId = 0;
    public List<Travel> objects;

    public AdapterResultSearch(Activity context,int textViewResourceId, List<Travel> objects) {
        super(context,textViewResourceId, objects);
        this.context = context;
        this.textViewResourceId = textViewResourceId;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rootView = null;
        if (rootView == null){
            LayoutInflater inflater = context.getLayoutInflater();
            rootView = inflater.inflate(textViewResourceId,null);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.name_result = (TextView) rootView.findViewById(R.id.name_result);
            viewHolder.city_result = (TextView) rootView.findViewById(R.id.city_result);
            viewHolder.type_result = (TextView) rootView.findViewById(R.id.type_result);
            viewHolder.price_result = (TextView) rootView.findViewById(R.id.price_result);
            viewHolder.image_result = (SmartImageView) rootView.findViewById(R.id.thumb_result);
            rootView.setTag(viewHolder);
        }else{
            rootView = convertView;
        }

        ViewHolder holder = (ViewHolder) rootView.getTag();
        String harga = objects.get(position).getHarga().toUpperCase();

        if (harga.length()>0){
            holder.price_result.setText("Rp. "+harga.format("%,d", Integer.parseInt(harga)));
        }else{
            holder.price_result.setText("");
        }
        holder.city_result.setText(objects.get(position).getCity().toUpperCase());
        holder.image_result.setImageUrl(objects.get(position).getUri_image());

        String type = objects.get(position).getType();
        int typing = Integer.parseInt(type);
        type = StaticVariable.type_travel(typing);
        holder.type_result.setText(type.toUpperCase());
        holder.name_result.setText(objects.get(position).getName().toUpperCase());
        return rootView;
    }
}
