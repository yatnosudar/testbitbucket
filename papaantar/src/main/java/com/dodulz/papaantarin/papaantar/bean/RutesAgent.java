package com.dodulz.papaantarin.papaantar.bean;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dodulz on 4/10/14.
 */
@DatabaseTable(tableName = "rutesagent")
public class RutesAgent {
    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField
    public String tipe;
    @DatabaseField
    public String asal;
    @DatabaseField
    public String tujuan1;
    @DatabaseField
    public String tujuan2;
    @DatabaseField
    public String tujuan3;
    @DatabaseField
    public String harga;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getAsal() {
        return asal;
    }

    public void setAsal(String asal) {
        this.asal = asal;
    }

    public String getTujuan1() {
        return tujuan1;
    }

    public void setTujuan1(String tujuan1) {
        this.tujuan1 = tujuan1;
    }

    public String getTujuan2() {
        return tujuan2;
    }

    public void setTujuan2(String tujuan2) {
        this.tujuan2 = tujuan2;
    }

    public String getTujuan3() {
        return tujuan3;
    }

    public void setTujuan3(String tujuan3) {
        this.tujuan3 = tujuan3;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }
}
