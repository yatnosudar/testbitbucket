package com.dodulz.papaantarin.papaantar.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dodulz on 3/5/14 AD.
 */
public class StaticVariable {
    public static String BASE_URL = "http://papaantar.in/api/v1/";
    public static String PLACE = BASE_URL+"place/";
    public static String ROUTE = BASE_URL+"search/";
    public static String VENDOR = BASE_URL+"search/travel/";
    public static String BYSLUG = BASE_URL+"detail/slug/";
    public static String BYVENDOR = BASE_URL+"search/place/";

    public static List<String> CITY = new ArrayList<String>();
    public static List<String> AIRPORT = new ArrayList<String>();
    public static int STATE_PROCESS = 1;
    public static String city = "";
    public static void save_data(ProcessAsync processAsync){
        processAsync.processData();
    }
    public static String type_travel(int typing){

        String type = "";
        switch (typing){
            case 1:
                type = "SUTTLE CAR"; // 1 5 6 7 11 12 13 14 15
                break;
            case 2:
                type = "RENT CAR";  // 2 7 9 12 13 15
                break;
            case 3:
                type = "CARTER"; // 3 6 7 9 12 15 13
                break;
            case 4:
                type = "SUTTLE BUS"; // 4 4 8 9 11 12 14 15
                break;
            case 5:
                type = "SUTTLE CAR, SUTTLE BUS";// 1 4
                break;
            case 6:
                type = "SUTTLE CAR, CARTER"; //1 3
                break;
            case 7:
                type = "Shuttle Car, Rent Car"; //1 2
                break;
            case 8:
                type = "Shuttle Bus, Carter"; //4 3
                break;
            case 9:
                type = "Shuttle Bus, Rent Car"; //4 2
                break;
            case 10:
                type = "Carter, Rent Car"; //3 2
                break;
            case 11:
                type = "Shuttle Car, Shuttle Bus, Carter"; //1 4 3
                break;
            case 12:
                type = "Shuttle Car, Shuttle Bus, Rent Car"; //1 4 2
                break;
            case 13:
                type = "Shuttle Car, Carter, Rent Car";// 1 3 2
                break;
            case 14:
                type = "Shuttle Bus, Carter, Rent Car";// 4 3 1
                break;
            case 15:
                type = "Shuttle Car, Shuttle Bus, Carter, Rent Car"; //1 4 2
                break;
        }
        return type;
    }

    public static String loadSavedPreferences(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String name = sharedPreferences.getString("city", "pilih kota");
        return name;
    }

    public static void savePreferences(Context context,String key, String value) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public static String getStringFromFile (Activity context,String filePath) throws Exception {
        AssetManager assetManager = context.getAssets();
//        File fl = new File(filePath);
//        FileInputStream fin = new FileInputStream(fl);
        InputStream inputStream = assetManager.open(filePath);
        String ret = convertStreamToString(inputStream);
//        fin.close();
        inputStream.close();
        return ret;
    }


    public static boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) MyApp.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }
}
