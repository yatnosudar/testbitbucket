package com.dodulz.papaantarin.papaantar.bean;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dodulz on 3/20/14.
 */
@DatabaseTable(tableName = "airport")
public class Airport {

    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField
    public String city;
    @DatabaseField
    public String code;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
