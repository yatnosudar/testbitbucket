package com.dodulz.papaantarin.papaantar.bean;

import android.content.Context;
import android.util.Log;

import com.dodulz.papaantarin.papaantar.util.DBHelp;
import com.dodulz.papaantarin.papaantar.util.DBManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by dodulz on 2/26/14 AD.
 */
public class AirportRepo {
    public DBHelp db;
    Dao<Airport,Integer> questionDao;

    public AirportRepo(Context context) {
        try {
            DBManager dbManager = new DBManager();

            db = dbManager.getDbHelp(context);
            questionDao = db.getAirportsDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int create(Airport mediaModel){
        try {
            return questionDao.create(mediaModel);
        }catch (SQLException e){
            Log.e(AirportRepo.class.toString(), e.getMessage());
        }
        return 0;
    }

    public int delete_all(){
        try {
            return questionDao.delete(getAll());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public int update(Airport mediaModel){
        try {
            return questionDao.delete(mediaModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List getAll(){
        try {
            return questionDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getCode(String city){
        try {
            return questionDao.queryBuilder().where().eq("city",city).queryForFirst().getCode();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
