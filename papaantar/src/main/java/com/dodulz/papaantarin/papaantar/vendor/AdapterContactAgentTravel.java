package com.dodulz.papaantarin.papaantar.vendor;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.bean.Contact;

import java.util.List;

/**
 * Created by dodulz on 4/10/14.
 */
public class AdapterContactAgentTravel extends ArrayAdapter<Contact> {
    private Activity context;
    private int resource;
    private List<Contact> objects;

    public AdapterContactAgentTravel(Activity context, int resource, List<Contact> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }

    static class  ViewHolder {
        TextView txt_key;
        TextView txt_value;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rootView = null;
        if (rootView == null){
            LayoutInflater inflater = context.getLayoutInflater();
            rootView = inflater.inflate(resource,null);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.txt_key = (TextView) rootView.findViewById(R.id.id_key);
            viewHolder.txt_value = (TextView) rootView.findViewById(R.id.id_val);
            rootView.setTag(viewHolder);
        }else{
            rootView = convertView;

        }
        ViewHolder holder = (ViewHolder) rootView.getTag();
        holder.txt_key.setText(objects.get(position).getType_contact());
        holder.txt_value.setText(objects.get(position).getNo_contact());
        return rootView;
    }
}
