package com.dodulz.papaantarin.papaantar.util;

import android.content.Context;

/**
 * Created by dodulz on 3/21/14.
 */
public class App {
    private static App ourInstance=null;
    private static MyApp myApp = null;

    public static App getInstance() {
        if (ourInstance==null){
            myApp = new MyApp();
        }
        return ourInstance;
    }

    private App() {
    }

    public Context getContext(){
        return myApp.getApplicationContext();
    }
}
