package com.dodulz.papaantarin.papaantar.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.adapter.AdapterResultSearchPlace;
import com.dodulz.papaantarin.papaantar.bean.Travel;
import com.dodulz.papaantarin.papaantar.bean.TravelRepo;
import com.dodulz.papaantarin.papaantar.util.AsyncSAVETravel;
import com.dodulz.papaantarin.papaantar.util.MyApp;
import com.dodulz.papaantarin.papaantar.util.StaticVariable;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by dodulz on 3/13/14 AD.
 */
public class PlaceFragment extends Fragment implements LocationListener{
    private LinearLayout linearLayout;
    private ListView listView;
    private TextView textView;
    private ProgressBar x_load;
    private AdapterResultSearchPlace adapterResultSearch;
    private LocationManager locationManager;
    private TravelRepo travelRepo ;
    private String provider="";

    public static String KOTA = "";


    MenuItem searchItem ;
    SearchView searchView ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.place_fragment,container,false);
        setHasOptionsMenu(true);
        ActionBar actionBar= ((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBar.setTitle("Vendor");


        travelRepo = new TravelRepo(MyApp.getInstance());

        listView = (ListView) v.findViewById(R.id.place_list);
        textView = (TextView) v.findViewById(R.id.city_list);
        linearLayout = (LinearLayout)v.findViewById(R.id.lin_city);
        x_load = (ProgressBar)v.findViewById(R.id.x_load);

        textView.setText(StaticVariable.loadSavedPreferences(MyApp.getInstance()).toUpperCase());
        adapterResultSearch = new AdapterResultSearchPlace(getActivity(),R.layout.item_result_search,GETDATA(StaticVariable.loadSavedPreferences(MyApp.getInstance())));

        if(StaticVariable.isOnline()) {
            if (KOTA.trim().length() == 0) {
                if (GETDATA(StaticVariable.loadSavedPreferences(MyApp.getInstance())).size() == 0
                        ||
                        StaticVariable.loadSavedPreferences(MyApp.getInstance()).equals("pilih kota")) {
                    x_load.setVisibility(View.VISIBLE);
                }else{

                }
                new BG_PROCESS_LOCATION().execute();
            }
        }

        listView.setAdapter(adapterResultSearch);
        listView.setTextFilterEnabled(true);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (searchItem!=null){
//                    searchItem.collapseActionView();
                    MenuItemCompat.collapseActionView(searchItem);
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                // Get the layout inflater
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View v = inflater.inflate(R.layout.place_dialog, null);

                builder.setView(v);
                final AutoCompleteTextView t = (AutoCompleteTextView) v.findViewById(R.id.kota);
//                t.setText(StaticVariable.loadSavedPreferences(MyApp.getInstance()));
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                        android.R.layout.simple_list_item_1, StaticVariable.CITY);
                t.setAdapter(adapter);
                builder.setPositiveButton("Cari", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        textView.setText(t.getText().toString().toUpperCase());
                        x_load.setVisibility(View.VISIBLE);
                        StaticVariable.savePreferences(MyApp.getInstance(),"city",t.getText().toString());
                        new BG_PROCESS(t.getText().toString()).execute();
                    }
                });
                builder.show();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Travel t = adapterResultSearch.getItem(i);
                String slugname = t.getSlug_name();
                String travel = t.getName();
                String city = t.getCity();
                String type_travel = t.getType();
                String state = t.getState();
                String country = t.getCountry();
                String facilities = t.getFacilities();
                String address = t.getAddress();
                String logo = t.getUri_image();

                Intent ix = new Intent(MyApp.getInstance(),DetailActivity.class);
                ix.putExtra("slug_name",slugname);
                ix.putExtra("travel",travel);
                ix.putExtra("city",city);
                ix.putExtra("type_travel",type_travel);
                ix.putExtra("state",state);
                ix.putExtra("country",country);
                ix.putExtra("facilities",facilities);
                ix.putExtra("address",address);
                ix.putExtra("logo",logo);
                ix.putExtra("status",1);
                ix.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(ix);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Travel t = adapterResultSearch.getItem(i);
                new AlertDialogCall(getActivity(),t.getSlug_name(),1).show();
                return false;
            }
        });



        return v;
    }

    public List<Travel> GETDATA(String city){
        return travelRepo.getAll(3,city);
    }

    @Override
    public void onLocationChanged(Location location) {

        double lat = location.getLatitude();
        double lng = location.getLongitude();

        Geocoder geoCoder = new Geocoder(MyApp.getInstance(), Locale.getDefault());
        StringBuilder builder = new StringBuilder();
        try {
            List<Address> address = geoCoder.getFromLocation(lat, lng, 1);
                if (address.size()>0){
                        String addressStr = address.get(0).getAddressLine(2);
                        builder.append(addressStr);
                        builder.append(" ");
                }
            if (StaticVariable.STATE_PROCESS==1){

                String[] parts = builder.toString().split(" ");
                if (!StaticVariable.loadSavedPreferences(MyApp.getInstance()).equals(parts[0])){
                    StaticVariable.savePreferences(MyApp.getInstance(),"city",parts[0].replace(",","").trim());
                }
                StaticVariable.STATE_PROCESS=2;
            }
        } catch (IOException e) {
            StaticVariable.STATE_PROCESS=2;
        }
        catch (NullPointerException e) {
            StaticVariable.STATE_PROCESS=2;
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onPause() {
        super.onPause();
        new BG_PROCESS_LOCATION().cancel(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_place_fragment, menu);
        searchItem = menu.findItem(R.id.action_search_place);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    Log.d("on Submit", s);
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    if (adapterResultSearch!=null){
                        adapterResultSearch.getFilter().filter(s);
                    }
                    return true;
                }
            });

//        menu.findItem(R.id.agentx).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem menuItem) {
//                StaticVariable.STATE_PROCESS=2;
//                startActivity(new Intent(getActivity(),VendorAgent.class));
//                return false;
//            }
//        });
//
//        menu.findItem(R.id.tentangx).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem menuItem) {
//                StaticVariable.STATE_PROCESS=2;
//                startActivity(new Intent(getActivity(),Tentang.class));
//                return false;
//            }
//        });
        super.onCreateOptionsMenu(menu, inflater);
    }



    class BG_PROCESS_LOCATION extends AsyncTask<Void,Void,Boolean>{
        @Override
        protected Boolean doInBackground(Void... voids) {

            locationManager = (LocationManager) MyApp.getInstance().getSystemService(Context.LOCATION_SERVICE);
            if (locationManager!=null){
                Criteria criteria = new Criteria();
                provider = locationManager.getBestProvider(criteria, false);
                final Location location = locationManager.getLastKnownLocation(provider);
                if (location != null) {
                    if (StaticVariable.STATE_PROCESS==1){
                                onLocationChanged(location);
                     }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            KOTA = StaticVariable.loadSavedPreferences(MyApp.getInstance()).toUpperCase();
            textView.setText(StaticVariable.loadSavedPreferences(MyApp.getInstance()).toUpperCase());
            if (GETDATA(StaticVariable.loadSavedPreferences(MyApp.getInstance())).size()==0
                    ||
                    StaticVariable.loadSavedPreferences(MyApp.getInstance()).equals("pilih kota")){
                x_load.setVisibility(View.VISIBLE);
                if (StaticVariable.isOnline()||StaticVariable.loadSavedPreferences(MyApp.getInstance())!="") {
                    new BG_PROCESS(StaticVariable.loadSavedPreferences(MyApp.getInstance())).execute();
                }
            }
        }
    }
    class BG_PROCESS extends AsyncTask<Void,Void,Boolean>{
        String data = "";
        public BG_PROCESS(String data){
            this.data = data;
        }
        @Override
        protected Boolean doInBackground(Void... voids) {
            String query_uri = "";
            String URLi = "";
            try {
                query_uri = URLEncoder.encode(data, "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            URLi = StaticVariable.BYVENDOR+ query_uri;
            final String finalURLi = URLi;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AsyncHttpClient ahc = new AsyncHttpClient();
                    Log.d("URI", finalURLi);

                    ahc.get(finalURLi, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(String content) {
                            try {
                                travelRepo.delete_all(3, data);
                                adapterResultSearch.clear();
                                adapterResultSearch.notifyDataSetChanged();
                                List<Travel> tra = new ArrayList<Travel>();
                                JSONArray jsonArray = new JSONArray(content);
                                JSONArray contactArray[] = new JSONArray[jsonArray.length()];
                                JSONArray armadaArray[] = new JSONArray[jsonArray.length()];
                                String slugArray[] = new String[jsonArray.length()];
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    Travel travel = new Travel();
                                    travel.setAddress(jsonObject.getString("address"));
                                    travel.setStatus_query(3);
                                    travel.setCity(jsonObject.getString("city"));
                                    travel.setSlug_name(jsonObject.getString("slug"));
                                    travel.setHarga("");
                                    travel.setCountry(jsonObject.getString("country"));
                                    travel.setName(jsonObject.getString("name"));
                                    travel.setFacilities(jsonObject.getString("facilities"));
                                    travel.setState(jsonObject.getString("state"));
                                    travel.setType(jsonObject.getString("type"));
                                    travel.setUri_image(jsonObject.getString("logo"));
                                    travelRepo.create(travel);
                                    adapterResultSearch.add(travel);
                                    tra.add(travel);
                                    contactArray[i] = jsonObject.getJSONArray("contacts");
                                    armadaArray[i] = jsonObject.getJSONArray("armadas");
                                    slugArray[i] = jsonObject.getString("slug");

                                }
                                adapterResultSearch.notifyDataSetChanged();
                                adapterResultSearch.update(tra);
                                x_load.setVisibility(View.INVISIBLE);
                                new AsyncSAVETravel(MyApp.getInstance(), contactArray, armadaArray, slugArray).execute();
                            } catch (JSONException e) {
                                Log.e("JSON ERROR", e.getMessage());
                            }

                        }

                        @Override
                        public void onFailure(Throwable error) {
                            x_load.setVisibility(View.INVISIBLE);
                            Toast.makeText(MyApp.getInstance(), "Terjadi kesalahan sistem...", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            StaticVariable.STATE_PROCESS=2;
        }
    }

}
