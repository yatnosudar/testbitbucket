package com.dodulz.papaantarin.papaantar;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

import com.dodulz.papaantarin.papaantar.tiketcom.SearchFlightFragment;

/**
 * Created by dodulz on 3/5/14 AD.
 */
public class TiketActivity extends ActionBarActivity {

    public int FROM_SEARCH_FRAGMENT = 0;

    public TiketActivity() {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.anim_in,R.anim.anim_out);
        setContentView(R.layout.search_main);
        setTitle("CARI RUTE");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        FROM_SEARCH_FRAGMENT = Integer.valueOf(getIntent().getStringExtra("spot"));
        if (savedInstanceState == null) {
           switch (FROM_SEARCH_FRAGMENT){
               case 0:
                   getSupportFragmentManager().beginTransaction()
                           .add(R.id.container, new SearchFlightFragment())
                           .commit();
                   break;
               case 1:
                   getSupportFragmentManager().beginTransaction()
                           .add(R.id.container, new SearchFlightFragment())
                           .commit();
                   break;
               case 2:
                   getSupportFragmentManager().beginTransaction()
                           .add(R.id.container, new SearchFlightFragment())
                           .commit();
                   break;
           }


        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}


