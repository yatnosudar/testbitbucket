package com.dodulz.papaantarin.papaantar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

import com.dodulz.papaantarin.papaantar.fragment.ResultSearchFragment;
import com.dodulz.papaantarin.papaantar.fragment.SearchFragment;

/**
 * Created by dodulz on 3/5/14 AD.
 */
public class SearchActivity extends ActionBarActivity {

    public static int FROM_SEARCH_FRAGMENT = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.anim_in,R.anim.anim_out);
        setContentView(R.layout.search_main);
        setTitle("CARI RUTE");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new SearchFragment())
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (FROM_SEARCH_FRAGMENT==2){
            ResultSearchFragment.RESULT_POSITION = 0;
            getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new SearchFragment())
                .commit();
        }else{
            ResultSearchFragment.RESULT_POSITION = 0;
            this.overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
            startActivity(new Intent(SearchActivity.this,NewDesignActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        }
    }

}


