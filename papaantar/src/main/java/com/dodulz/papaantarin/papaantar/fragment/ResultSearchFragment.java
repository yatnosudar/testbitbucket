package com.dodulz.papaantarin.papaantar.fragment;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SpinnerAdapter;
import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.SearchActivity;
import com.dodulz.papaantarin.papaantar.adapter.AdapterResultSearch;
import com.dodulz.papaantarin.papaantar.bean.Travel;
import com.dodulz.papaantarin.papaantar.bean.TravelRepo;
import java.util.List;

/**
 * Created by dodulz on 3/9/14 AD.
 */
public class ResultSearchFragment extends Fragment implements ActionBar.OnNavigationListener{

    ListView listView ;
    ActionBar.OnNavigationListener mOnNavigationListener;
    AdapterResultSearch adapter;
    String query,berangkat,tiba,vendor = "";
    private SpinnerAdapter mSpinnerAdapter;
    public static int RESULT_POSITION = 0;
    TravelRepo travelRepo = new TravelRepo(getActivity());
    public ResultSearchFragment(String berangkat,String tiba,String vendor){

        if (berangkat!=""&&tiba!=""){
            query = berangkat+" - "+tiba;
        }else{
            query = vendor;
        }
        this.berangkat = berangkat;
        this.tiba = tiba;
        this.vendor = vendor;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SearchActivity.FROM_SEARCH_FRAGMENT = 0;
        View view = inflater.inflate(R.layout.main_result_search,container,false);
        getActivity().setTitle(query);
        ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        mSpinnerAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.action_list,
                android.R.layout.simple_spinner_dropdown_item);
        listView = (ListView) view.findViewById(R.id.list_search);
        adapter = new AdapterResultSearch(getActivity(),R.layout.item_result_search,GETDATA());
        listView.setAdapter(adapter);
        actionBar.setListNavigationCallbacks(mSpinnerAdapter,this);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String slugname = GETDATA().get(i).getSlug_name();
                String travel = GETDATA().get(i).getName();
                String city = GETDATA().get(i).getCity();
                String type_travel = GETDATA().get(i).getType();
                String state = GETDATA().get(i).getState();
                String country = GETDATA().get(i).getCountry();
                String facilities = GETDATA().get(i).getFacilities();
                String address = GETDATA().get(i).getAddress();


                Intent ix = new Intent(getActivity(),DetailActivity.class);
                ix.putExtra("slug_name",slugname);
                ix.putExtra("travel",travel);
                ix.putExtra("city",city);
                ix.putExtra("type_travel",type_travel);
                ix.putExtra("state",state);
                ix.putExtra("country",country);
                ix.putExtra("facilities",facilities);
                ix.putExtra("address",address);

                startActivity(ix);
            }
        });

        return view;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void updateList(int i){
        RESULT_POSITION = i;
        adapter.clear();
        adapter.notifyDataSetChanged();
        adapter.addAll(GETDATA());
        adapter.notifyDataSetChanged();
    }

    public List<Travel> GETDATA(){
        List<Travel> dt =  travelRepo.getWhereType(RESULT_POSITION,1);
        if (RESULT_POSITION==0){
            dt = new TravelRepo(getActivity()).getAll();
        }
        return dt;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onNavigationItemSelected(int i, long l) {
        updateList(i);
        return false;
    }
}
