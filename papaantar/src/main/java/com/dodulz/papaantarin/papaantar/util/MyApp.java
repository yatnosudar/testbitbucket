package com.dodulz.papaantarin.papaantar.util;

import android.app.Application;

/**
 * Created by dodulz on 3/21/14.
 */
public class MyApp extends Application {

    private static MyApp singleton;

    public static MyApp getInstance(){
        return singleton;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
    }
}
