package com.dodulz.papaantarin.papaantar.adapter;

import android.app.Activity;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.bean.Contact;
import java.util.List;

/**
 * Created by dodulz on 3/9/14 AD.
 */
public class AdapterContactDetail extends ArrayAdapter<Contact> {
    Activity context;
    int resource = 0;
    List<Contact> objects;

    static class ViewHolder{
        public TextView txt_key;
        public TextView txt_value;
    }
    public AdapterContactDetail(Activity context, int resource, List<Contact> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rootView = null;
        if (rootView == null){
            LayoutInflater inflater = context.getLayoutInflater();
            rootView = inflater.inflate(resource,null);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.txt_key = (TextView) rootView.findViewById(R.id.id_key);
            viewHolder.txt_value = (TextView) rootView.findViewById(R.id.id_val);
            rootView.setTag(viewHolder);
        }else{
            rootView = convertView;
        }
        ViewHolder holder = (ViewHolder) rootView.getTag();
        int type = Integer.parseInt(objects.get(position).getType_contact());
        String tipe = "";
        switch (type){
            case 1:
                holder.txt_value.setAutoLinkMask(Linkify.PHONE_NUMBERS);
                tipe = "PONSEL";
                break;
            case 2:
                holder.txt_value.setAutoLinkMask(Linkify.WEB_URLS);
                tipe = "WEBSITE";
                break;
            case 3:
                holder.txt_value.setAutoLinkMask(Linkify.PHONE_NUMBERS);
                tipe = "TELEPON";
                break;
        }
        holder.txt_key.setText(tipe);
        holder.txt_value.setText(objects.get(position).getNo_contact());
        return rootView;
    }
}
