package com.dodulz.papaantarin.papaantar.vendor;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.bean.Agent;

/**
 * Created by dodulz on 4/8/14.
 */
public class BiodataVendorFragment extends Fragment {

    private Spinner tipe;
    private EditText nama, alamat, kota, provinsi, fasilitas, deskripsi;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.register_agent, container, false);
        setHasOptionsMenu(true);
        ActionBar actionBar= ((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBar.setTitle("Biodata");
        nama = (EditText) view.findViewById(R.id.nama_travel);
        alamat = (EditText) view.findViewById(R.id.alamat_travel);
        kota = (EditText) view.findViewById(R.id.kota_travel);
        provinsi = (EditText) view.findViewById(R.id.provinsi_travel);
        fasilitas = (EditText) view.findViewById(R.id.fasilitas_travel);
        deskripsi = (EditText) view.findViewById(R.id.deskripsi_travel);
        tipe = (Spinner) view.findViewById(R.id.tipe_travel);

        init();
        return view;
    }

    public void init(){
        Agent agent = DataAgentRegister.AGENT_BIODATA;
        if (agent.getNama()!=null){
            nama.setText(agent.getNama());
        }
        if (agent.getAlamat()!=null){
            alamat.setText(agent.getNama());
        }
        if (agent.getKota()!=null){
            kota.setText(agent.getKota());
        }
        if (agent.getProvinsi()!=null){
            provinsi.setText(agent.getProvinsi());
        }
        if (agent.getFasilitas()!=null){
            fasilitas.setText(agent.getFasilitas());
        }
        if (agent.getDeskripsi()!=null){
            deskripsi.setText(agent.getDeskripsi());
        }
        if (agent.getTipe()!=null){
            tipe.setSelection(getIndex(agent.getTipe()));
        }
    }

    private int getIndex(String string){
        int index = 0;
        String[] arr = getResources().getStringArray(R.array.data_travel);
        for (int i = 0; i < arr.length; i++){
            if (tipe.getItemAtPosition(i).equals(string)){
                index = i;
            }
        }
        return index;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.vendor_agent, menu);
        menu.findItem(R.id.action_save_vendor).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if (add_biodata()) {
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, new RuteVendorFragment())
                            .commit();
                }
                return false;
            }
        });
        menu.findItem(R.id.action_kembali_vendor).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                    getActivity().finish();
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean add_biodata() {
        StringBuffer bf = new StringBuffer();
        int i = 0;
        if (nama.getText().toString().length()>0) {
            DataAgentRegister.AGENT_BIODATA.setNama(nama.getText().toString());
            i++;
        } else {
            bf.append("Nama Wajib Diisi \n");
        }

//        if (tipe.getSelectedItem().toString().length()>0) {
//            DataAgentRegister.AGENT_BIODATA.setTipe(tipe.getSelectedItem().toString());
//            i++;
//        } else {
//            bf.append("Tipe Wajib Dipilih");
//        }

        if (deskripsi.getText().toString().length()>0) {
            DataAgentRegister.AGENT_BIODATA.setDeskripsi(deskripsi.getText().toString());
            i++;
        } else {
            bf.append("Deskripsi Wajib Diisi \n");
        }

        if (fasilitas.getText().toString().length()>0) {
            DataAgentRegister.AGENT_BIODATA.setFasilitas(fasilitas.getText().toString());
            i++;
        } else {
            bf.append("Fasilitas Wajib Diisi \n");
        }

        if (provinsi.getText().toString().length()>0) {
            DataAgentRegister.AGENT_BIODATA.setProvinsi(provinsi.getText().toString());
            i++;
        } else {
            bf.append("Provinsi Wajib Diisi \n");
        }

        if (kota.getText().toString().length()>0) {
            DataAgentRegister.AGENT_BIODATA.setKota(kota.getText().toString());
            i++;
        } else {
            bf.append("Kota Wajib Diisi \n");
        }

        if (alamat.getText().toString().length()>0) {
            DataAgentRegister.AGENT_BIODATA.setAlamat(alamat.getText().toString());
            i++;
        } else {
            bf.append("Alamat Wajib Diisi \n");
        }

        DataAgentRegister.AGENT_BIODATA.setTipe(tipe.getSelectedItem().toString());
        if (i==6){
            return true;
        }else{
            Toast.makeText(getActivity(), bf.toString(), Toast.LENGTH_LONG).show();
            return false;
        }
    }


}
