package com.dodulz.papaantarin.papaantar.bean;

import android.content.Context;
import android.util.Log;

import com.dodulz.papaantarin.papaantar.util.DBHelp;
import com.dodulz.papaantarin.papaantar.util.DBManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by dodulz on 2/26/14 AD.
 */
public class RutesRepo {
    public DBHelp db;
    Dao<Rutes,Integer> questionDao;

    public RutesRepo(Context context) {
        try {
            DBManager dbManager = new DBManager();

            db = dbManager.getDbHelp(context);
            questionDao = db.getRutessDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int create(Rutes mediaModel){
        try {
            Log.e(Rutes.class.toString(), "Success Save");
            return questionDao.create(mediaModel);
        }catch (SQLException e){
            Log.e(RutesRepo.class.toString(), e.getMessage());
        }
        return 0;
    }

    public int delete_all(String slug){
        try {
            return questionDao.delete(getAllWhere(slug));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public int delete_all(){
        try {
            return questionDao.delete(getAll());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public int update(Rutes mediaModel){
        try {
            return questionDao.delete(mediaModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public List getAll(int i){
        try {
            return questionDao.queryBuilder().where().eq("status_query",i).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public List getAll(){
        try {
            return questionDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public List getAllWhere(String slug){
        try {
            return questionDao.queryBuilder().where().eq("slug",slug).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List getAllLimit(){
        try {
            return questionDao.queryBuilder().limit(10).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
