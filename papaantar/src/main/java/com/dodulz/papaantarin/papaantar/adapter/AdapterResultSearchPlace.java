package com.dodulz.papaantarin.papaantar.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.bean.Travel;
import com.dodulz.papaantarin.papaantar.util.StaticVariable;
import com.loopj.android.image.SmartImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dodulz on 3/7/14 AD.
 */
public class AdapterResultSearchPlace extends ArrayAdapter<Travel>{

    static class ViewHolder{
        public TextView name_result;
        public TextView city_result;
        public TextView type_result;
        public TextView price_result;
        public SmartImageView image_result;
    }

    public Activity context;
    public int textViewResourceId = 0;

    private List<Travel> valuesPoi;
    private List<Travel> filteredPoi;
    private FILTER mFilter;


    public AdapterResultSearchPlace(Activity context, int textViewResourceId, List<Travel> objects) {
        super(context,textViewResourceId, objects);
        this.context = context;
        this.textViewResourceId = textViewResourceId;
        this.valuesPoi =new ArrayList<Travel>(objects);
        this.filteredPoi = new ArrayList<Travel>(objects);
        this.mFilter =  new FILTER();
    }

    public void update(List<Travel> objects){
        this.valuesPoi =new ArrayList<Travel>(objects);
        this.filteredPoi = new ArrayList<Travel>(objects);
        this.mFilter =  new FILTER();
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rootView = null;
            if (rootView == null){
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                rootView = inflater.inflate(textViewResourceId,null);
                final ViewHolder viewHolder = new ViewHolder();
                viewHolder.name_result = (TextView) rootView.findViewById(R.id.name_result);
                viewHolder.city_result = (TextView) rootView.findViewById(R.id.city_result);
                viewHolder.type_result = (TextView) rootView.findViewById(R.id.type_result);
                viewHolder.price_result = (TextView) rootView.findViewById(R.id.price_result);
                viewHolder.image_result = (SmartImageView) rootView.findViewById(R.id.thumb_result);
                rootView.setTag(viewHolder);
            }else{
                rootView = convertView;
            }

            ViewHolder holder = (ViewHolder) rootView.getTag();

            Travel p;
            if (filteredPoi.size()>0){
                p = filteredPoi.get(position);
            }else{
                p = valuesPoi.get(position);
            }
            String harga = p.getHarga().toUpperCase();
            if (harga.length()>0){
                holder.price_result.setText("Rp. "+harga.format("%,d", Integer.parseInt(harga)));
            }else{
                holder.price_result.setText("");
            }
            holder.city_result.setText(p.getCity().toUpperCase());
            holder.image_result.setImageUrl(p.getUri_image());

            String type = p.getType();
            int typing = Integer.parseInt(type);
            type = StaticVariable.type_travel(typing);
            holder.type_result.setText(type.toUpperCase());
            holder.name_result.setText(p.getName().toUpperCase());

        return rootView;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new FILTER();
        }
        return mFilter;
    }

    class FILTER extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            FilterResults results = new FilterResults();
            charSequence = charSequence.toString().toLowerCase();

            if (charSequence==null || charSequence.length()==0){
                synchronized (this){
                    results.values = valuesPoi;
                    results.count = valuesPoi.size();
                }
            }
            else{
                final List<Travel> filterList = new ArrayList<Travel>();
                int count = valuesPoi.size();
                for (int i = 0; i < count; i++) {
                    Travel p = valuesPoi.get(i);
                    if (p.toString().toLowerCase().contains(charSequence)){
                        filterList.add(p);
                    }
                }
                results.values = filterList;
                results.count = filterList.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            filteredPoi = (List<Travel>) filterResults.values;
            notifyDataSetChanged();
            clear();
            int count = filteredPoi.size();
            for (int i = 0; i < count; i++) {
                add(filteredPoi.get(i));
                notifyDataSetInvalidated();
            }
        }
    }
}
