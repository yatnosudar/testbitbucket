package com.dodulz.papaantarin.papaantar.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.SearchActivity;
import com.dodulz.papaantarin.papaantar.util.StaticVariable;

/**
 * Created by dodulz on 3/5/14 AD.
 */
public class SearchFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SearchActivity.FROM_SEARCH_FRAGMENT = 1;
        View view = inflater.inflate(R.layout.search, container, false);
        ActionBar actionBar= ((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setTitle("Cari Travel");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, StaticVariable.CITY);
        final AutoCompleteTextView berangkat = (AutoCompleteTextView)
                view.findViewById(R.id.berangkat);
        final AutoCompleteTextView tiba = (AutoCompleteTextView)
                view.findViewById(R.id.tiba);

        final TextView vendor = (TextView) view.findViewById(R.id.vendor);
        berangkat.setAdapter(adapter);
        tiba.setAdapter(adapter);

        Button cari = (Button) view.findViewById(R.id.button_cari);
        cari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (berangkat.getText().length()>0 &&
                        tiba.getText().length()>0){

                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container_main, new RSearchFragment(
                                        berangkat.getText().toString(),
                                        tiba.getText().toString(),
                                        vendor.getText().toString()))
                                .commit();
                }else {
                    Toast.makeText(getActivity(),"Field Harus Disi",Toast.LENGTH_LONG).show();
                    berangkat.setTextColor(Color.WHITE);
                    tiba.setTextColor(Color.WHITE);

                    berangkat.setBackgroundColor(getResources().getColor(R.color.orange));
                    tiba.setBackgroundColor(getResources().getColor(R.color.orange));
                    berangkat.setHighlightColor(getResources().getColor(R.color.orange));
                    tiba.setHighlightColor(getResources().getColor(R.color.orange));
                }
            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        menu.clear();
    }

}
