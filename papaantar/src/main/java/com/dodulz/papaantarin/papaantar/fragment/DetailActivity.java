package com.dodulz.papaantarin.papaantar.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dodulz.papaantarin.papaantar.NewDesignActivity;
import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.VendorAgent;
import com.dodulz.papaantarin.papaantar.about.Agent;
import com.dodulz.papaantarin.papaantar.about.Tentang;
import com.dodulz.papaantarin.papaantar.adapter.AdapterArmadaDetail;
import com.dodulz.papaantarin.papaantar.bean.Armada;
import com.dodulz.papaantarin.papaantar.bean.ArmadaRepo;
import com.dodulz.papaantarin.papaantar.bean.Contact;
import com.dodulz.papaantarin.papaantar.bean.ContactRepo;
import com.dodulz.papaantarin.papaantar.bean.Travel;
import com.dodulz.papaantarin.papaantar.bean.TravelRepo;
import com.dodulz.papaantarin.papaantar.tiketcom.TIKETCOM_API;
import com.dodulz.papaantarin.papaantar.tiketcom.WebTiket;
import com.dodulz.papaantarin.papaantar.util.AsyncDETAILTravel;
import com.dodulz.papaantarin.papaantar.util.StaticVariable;
import com.loopj.android.image.SmartImageView;

import java.util.List;

/**
 * Created by dodulz on 3/9/14 AD.
 */
public class DetailActivity extends ActionBarActivity {
//
//    private ShareActionProvider mShareActionProvider;
//    ListView contact,armada;
//    AdapterContactDetail adapter_contact;
    ListView armada;
    AdapterArmadaDetail adapter_armada;
    Button vendor,search,favorit;

    String slug_name = "";
    String travel ="";
    String city ="";
    String type_travel ="";
    String state ="";
    String country = "";
    String facilities = "";
    String address = "";
    String logo = "";

    ContactRepo contacts = new ContactRepo(DetailActivity.this);
    ArmadaRepo armadas = new ArmadaRepo(DetailActivity.this);

    LinearLayout lin_reservasi,lin_all;
    ImageButton img_reservasi, img_armada;

    int state_nav = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(0, 0);
        Intent intent = getIntent();
        travel = intent.getStringExtra("travel");
        setTitle(travel);
        slug_name = intent.getStringExtra("slug_name");
        city = intent.getStringExtra("city");
        type_travel = intent.getStringExtra("type_travel");
        state = intent.getStringExtra("state");
        country = intent.getStringExtra("country");
        facilities = intent.getStringExtra("facilities");
        address = intent.getStringExtra("address");
        logo = intent.getStringExtra("logo");



        setContentView(R.layout.detail_vendor);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

        lin_reservasi = (LinearLayout) findViewById(R.id.lin_reservasi);
        img_reservasi = (ImageButton) findViewById(R.id.nav_expad_reservasi);
        lin_all = (LinearLayout) findViewById(R.id.lin_all);


        name_detail().setText(travel);
        city_detail().setText(city);
        city2_detail().setText(city);
        type_detail().setText(StaticVariable.type_travel(Integer.parseInt(type_travel)));
        address_detail().setText(address);
        state_detail().setText(state);
        facility_detail().setText(Html.fromHtml(facilities));
        smartImageView().setImageUrl(logo);


//        adapter_contact = new AdapterContactDetail(DetailActivity.this,R.layout.item_key_val,contacts());
//        contact = (ListView) findViewById(R.id.reservasi_info);
//        LinearLayout.LayoutParams lpx = (LinearLayout.LayoutParams) contact.getLayoutParams();
//        lpx.height = 80*contacts().size();
//        contact.setLayoutParams(lpx);
//        contact.setAdapter(adapter_contact);
//
        adapter_armada = new AdapterArmadaDetail(DetailActivity.this,R.layout.item_key_val,armadas());
        armada = (ListView) findViewById(R.id.armada_detail);
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) armada.getLayoutParams();
        lp.height = 80*armadas().size();
        armada.setLayoutParams(lp);
        armada.setAdapter(adapter_armada);

        init();
        img_reservasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (state_nav==1){
                    lin_all.setVisibility(View.VISIBLE);
                    img_reservasi.setImageDrawable(getResources().getDrawable(R.drawable.navigation_collapse));
                    state_nav = 0;
                }else{
                    lin_all.setVisibility(View.GONE);
                    img_reservasi.setImageDrawable(getResources().getDrawable(R.drawable.navigation_expand));
                    state_nav = 1;
                }
            }
        });
    }


    public TextView name_detail(){
        return (TextView) findViewById(R.id.namex_detail);
    }
    public TextView city_detail(){
        return (TextView) findViewById(R.id.city_detail);
    }
    public TextView city2_detail(){
        return (TextView) findViewById(R.id.city2_detail);
    }
    public TextView type_detail(){
        return (TextView) findViewById(R.id.type_detail);
    }
    public TextView address_detail(){
        return (TextView) findViewById(R.id.address_detail);
    }
    public TextView state_detail(){
        return (TextView) findViewById(R.id.state_detail);
    }
    public TextView facility_detail(){
        return (TextView) findViewById(R.id.fasilitas_detail);
    }
    public TextView tel_show(){return (TextView) findViewById(R.id.tel_show);}
    public TextView pon_show(){return (TextView) findViewById(R.id.ponsel_show);}
    public TextView web_show(){return (TextView) findViewById(R.id.website_show);}
    public TextView tel_title(){return (TextView) findViewById(R.id.telpon_title);}
    public TextView pon_title(){return (TextView) findViewById(R.id.ponsel_title);}
    public TextView web_title(){return (TextView) findViewById(R.id.website_title);}
    public TextView armada_show(){return (TextView) findViewById(R.id.armada_show);}
    public LinearLayout lin_armada(){return (LinearLayout) findViewById(R.id.lin_armada);}
    public LinearLayout lin_fasilitas(){return (LinearLayout) findViewById(R.id.linear_fasilitas);}
    public LinearLayout lin_reservasi(){return (LinearLayout)findViewById(R.id.lin_reservasi);}


    public void init(){
        Intent zi = getIntent();
        int zix = zi.getIntExtra("status",1);

        vendor = (Button) findViewById(R.id.image_vendor);
        search = (Button) findViewById(R.id.image_search);
        favorit = (Button) findViewById(R.id.image_bookmark);

        if (zix==1){
            vendor.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.vendor_active), null, null);
            vendor.setTextColor(getResources().getColor(R.color.orange));
        }
        else if(zix==2){
            search.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.search_active), null, null);
            search.setTextColor(getResources().getColor(R.color.orange));
        }
        else if(zix==3){
            favorit.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.bookmark_active), null, null);
            favorit.setTextColor(getResources().getColor(R.color.orange));
        }

        List<Contact> contacts1 = contacts();
        StringBuffer telepon = new StringBuffer();
        StringBuffer ponsel = new StringBuffer();
        StringBuffer website = new StringBuffer();
        for (int ix = 0; ix<contacts1.size();ix++){
            switch (Integer.parseInt(contacts1.get(ix).getType_contact())){
                case 1:
                    ponsel.append(contacts1.get(ix).getNo_contact()+"\n");
                    break;
                case 2:
                    website.append(contacts1.get(ix).getNo_contact() + "\n");
                    break;
                case 3:
                    telepon.append(contacts1.get(ix).getNo_contact()+"\n");
                    break;
            }
        }

        int x = 0;
        if (telepon.length()<1){
            tel_title().setVisibility(View.GONE);
            x++;
        }
        if (ponsel.length()<1){
            pon_title().setVisibility(View.GONE);
            x++;
        }
        if (website.length()<1){
            web_title().setVisibility(View.GONE);
            x++;
        }


        tel_show().setText(telepon.toString());
        pon_show().setText(ponsel.toString());
        web_show().setText(website.toString());

        if (x==3){
            lin_reservasi().setVisibility(View.GONE);
        }
        List<Armada> armadas1 = armadas();
        StringBuffer arm = new StringBuffer();
        for (int i=0;i< armadas1.size();i++){
             arm.append(armadas1.get(i).getArmada());
             if (i<armadas1.size()-1){
                 arm.append(", ");
             }
        }
        if (arm.toString().length()<1){
            lin_armada().setVisibility(View.GONE);
        }
        armada_show().setText(arm.toString());

        if (facilities==null||facilities.equals("null")||facilities.trim().length()<1){
            lin_fasilitas().setVisibility(View.GONE);
        }
    }

    public SmartImageView smartImageView(){

        return (SmartImageView) findViewById(R.id.smart_view);
    }

    public List<Contact> contacts(){
        List<Contact> armadasx = contacts.getAllWhere(slug_name,1);
        if (armadasx!=null){
            return contacts.getAllWhere(slug_name,1);
        }else{
            Contact armada1 = new Contact();
            armada1.setType_contact("Contact Belum Tersedia");
            armada1.setNo_contact("");
            armadasx.add(armada1);
            return armadasx;
        }
    }
    public List<Armada> armadas(){
        List<Armada> armadasx = armadas.getAllWhere(slug_name,1);
        if (armadasx!=null){
            return armadasx;
        }else{
            Armada armada1 = new Armada();
            armada1.setArmada("Armada Belum Tersedia");
            armada1.setTotal("");
            armadasx.add(armada1);
            return armadasx;
        }
    }

    public void _clickRutes(View v){
        Intent zi = getIntent();
        int zix = zi.getIntExtra("status",1);
        new AsyncDETAILTravel(DetailActivity.this,slug_name,travel,zix).execute();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean navigateUpTo(Intent upIntent) {
        finish();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()){
            case R.id.agent:
                startActivity(new Intent(DetailActivity.this, Agent.class));
                break;
            case R.id.tentang:
                startActivity(new Intent(DetailActivity.this, Tentang.class));
                break;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    // Call to update the share intent
    private Intent getDefaultIntent() {
            Intent intent=new Intent(android.content.Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            intent.putExtra(Intent.EXTRA_SUBJECT, "Berbagi untuk "+travel);
            intent.putExtra(Intent.EXTRA_TEXT, "Cari travel cepat, Traveling jadi Gampang PapaAntar.in http://papaantar.in/"+slug_name);
            return intent;
    }


    public void _call(View view){
        new AlertDialogCall(DetailActivity.this,slug_name,1).show();
    }

    public void _sms(View view){
        new AlertDialogCall(DetailActivity.this,slug_name,2).show();
    }

    public void _share(View view){
        Intent intent=new Intent(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Berbagi untuk "+travel);
        intent.putExtra(Intent.EXTRA_TEXT, "Cari travel cepat, Traveling jadi Gampang PapaAntar.in http://papaantar.in/"+slug_name);
        startActivity(Intent.createChooser(intent,"Berbagi untuk "+travel));

    }

    public void _save(View view){
        Travel t = new Travel();
        TravelRepo tr = new TravelRepo(DetailActivity.this);
        if (tr.getAllSlug(2, slug_name).size()<=0){
            t.setAddress(address);
            t.setState(state);
            t.setFacilities(facilities);
            t.setType(type_travel);
            t.setCountry(country);
            t.setHarga("");
            t.setSlug_name(slug_name);
            t.setName(travel);
            t.setCity(city);
            t.setUri_image(logo);
            t.setStatus_query(2);
            tr.create(t);
            Toast.makeText(getBaseContext(), "Simpan Travel", Toast.LENGTH_SHORT).show();
        }
    }


    public void _vendor(View v){
        Intent i = new Intent(DetailActivity.this, NewDesignActivity.class);
        i.putExtra("status",1);
        startActivity(i
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        finish();
        overridePendingTransition(0, 0);
    }

    public void _cari(View v){
        Intent i = new Intent(DetailActivity.this, NewDesignActivity.class);
        i.putExtra("status",2);
        startActivity(i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        finish();
        overridePendingTransition(0, 0);
    }

    public void _bookmark(View v){
        Intent i = new Intent(DetailActivity.this, NewDesignActivity.class);
        i.putExtra("status",3);
        startActivity(i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        finish();
        overridePendingTransition(0, 0);
    }

    public void _more(View v){
//        Intent i = new Intent(DetailActivity.this, NewDesignActivity.class);
//        i.putExtra("status",4);
//        startActivity(i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
//        finish();
//        overridePendingTransition(0, 0);
        final Dialog dialog = new Dialog(DetailActivity.this,R.style.DialogAnimation);
        dialog.getWindow().setWindowAnimations(R.style.DialogAnimation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_more);
        Button btn1 = (Button) dialog.findViewById(R.id.btn1);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (StaticVariable.isOnline()){
                    if (TIKETCOM_API.TOKEN.length()==0){
                        TIKETCOM_API.token();
                    }
                    startActivity(new Intent(DetailActivity.this, WebTiket.class).putExtra("status", "10"));

                    //getActivity().startActivity(new Intent(getActivity(), TiketActivity.class).putExtra("spot","0"));
                }else{
                    Toast.makeText(DetailActivity.this, "Tidak ada jaringan...", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });
        Button btn2 = (Button) dialog.findViewById(R.id.btn2);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (StaticVariable.isOnline()){

                    startActivity(new Intent(DetailActivity.this, WebTiket.class).putExtra("status", "3"));
                }else{
                    Toast.makeText(DetailActivity.this,"Tidak ada jaringan...",Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });
        Button btn3 = (Button) dialog.findViewById(R.id.btn3);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (StaticVariable.isOnline()){
                    startActivity(new Intent(DetailActivity.this, WebTiket.class).putExtra("status", "2"));
                }else{
                    Toast.makeText(DetailActivity.this,"Tidak ada jaringan...",Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });
        Button btn4 = (Button) dialog.findViewById(R.id.btn4);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DetailActivity.this,VendorAgent.class));
                dialog.dismiss();
            }
        });
        Button btn5 = (Button) dialog.findViewById(R.id.btn5);
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DetailActivity.this,Tentang.class));
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
