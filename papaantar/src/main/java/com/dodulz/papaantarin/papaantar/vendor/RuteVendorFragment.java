package com.dodulz.papaantarin.papaantar.vendor;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.bean.RutesAgent;
import com.dodulz.papaantarin.papaantar.util.StaticVariable;

/**
 * Created by dodulz on 4/8/14.
 */
public class RuteVendorFragment extends Fragment {

    private ListView rutes;
    AdapterRutesAgentTravel adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_register_vendor,container,false);
        setHasOptionsMenu(true);
        ActionBar actionBar= ((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBar.setTitle("Rute");
        rutes = (ListView) view.findViewById(R.id.list_vendor_register);
        adapter = new AdapterRutesAgentTravel(getActivity(),R.layout.item_rutes_agent,DataAgentRegister.AGENT_RUTES);
        rutes.setAdapter(adapter);
        listenerListview();
        return view;
    }

    public void listenerListview(){
        rutes.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                RutesAgent ra = DataAgentRegister.AGENT_RUTES.get(i);
                final int position =i;
                StringBuffer bf = new StringBuffer();
                if (ra.getTujuan1().length()>0){
                    bf.append(" - "+ra.getTujuan1());
                }
                if (ra.getTujuan2().length()>0){
                    bf.append(" - "+ra.getTujuan2());
                }
                if (ra.getTujuan3().length()>0){
                    bf.append(" - "+ra.getTujuan3());
                }
                builder.setTitle("Hapus data ");
                String harga = ra.getHarga();
                builder.setMessage(ra.getAsal()+" - "+bf.toString()+"\n Rp."+harga.format("%,d", Integer.parseInt(harga))+"\n"+ra.getTipe());
                builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        DataAgentRegister.AGENT_RUTES.remove(position);
                        adapter.notifyDataSetChanged();
                        dialogInterface.dismiss();
                    }
                });
                builder.setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.show();
                return false;
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_rute_vendor_register,menu);

        menu.findItem(R.id.action_next_rutes).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new ContactVendorFragment())
                        .commit();
                return false;
            }
        });
        menu.findItem(R.id.action_back_rutes).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new BiodataVendorFragment())
                        .commit();
                return false;
            }
        });
        menu.findItem(R.id.action_add_rutes).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_rute,null);
                builder.setView(view);
                builder.setTitle("Tambah Rute");
                final AutoCompleteTextView asal = (AutoCompleteTextView) view.findViewById(R.id.id_asal_dialog);
                final AutoCompleteTextView tujuan1 = (AutoCompleteTextView) view.findViewById(R.id.id_tujuan1);
                final AutoCompleteTextView tujuan2 = (AutoCompleteTextView) view.findViewById(R.id.id_tujuan2);
                final AutoCompleteTextView tujuan3 = (AutoCompleteTextView) view.findViewById(R.id.id_tujuan3);
                final TextView harga = (TextView) view.findViewById(R.id.id_harga_dialog);
                final Spinner tipe = (Spinner) view.findViewById(R.id.spinner_tipe);

                ArrayAdapter<String> adapter_auto = new ArrayAdapter<String>(getActivity(),
                        android.R.layout.simple_list_item_1, StaticVariable.CITY);

                asal.setAdapter(adapter_auto);
                tujuan1.setAdapter(adapter_auto);
                tujuan2.setAdapter(adapter_auto);
                tujuan3.setAdapter(adapter_auto);

                builder.setPositiveButton("Tambah",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (asal.getText().length()>0&&harga.getText().length()>0) {
                            RutesAgent ra = new RutesAgent();
                            ra.setAsal(asal.getText().toString());
                            ra.setHarga(harga.getText().toString());
                            ra.setTujuan1(tujuan1.getText().toString());
                            ra.setTujuan2(tujuan2.getText().toString());
                            ra.setTujuan3(tujuan3.getText().toString());
                            ra.setTipe(tipe.getSelectedItem().toString());

//                        adapter.add(ra);
                            adapter.notifyDataSetChanged();
                            DataAgentRegister.AGENT_RUTES.add(ra);
                            dialogInterface.dismiss();
                        }else{
                            Toast.makeText(getActivity(),"Asal dan Harga wajib di isi", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                builder.setNegativeButton("Batal",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.show();
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }


}
