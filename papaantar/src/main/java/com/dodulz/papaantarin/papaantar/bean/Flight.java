package com.dodulz.papaantarin.papaantar.bean;

/**
 * Created by dodulz on 3/21/14.
 */

public class Flight {

    public String flight_id;
    public String airlines_name;
    public String flight_number;
    public String price_value;

    public String full_via;
    public String duration;
    public String stop_flight;

    public String image_url;


    public String getFlight_id() {
        return flight_id;
    }

    public void setFlight_id(String flight_id) {
        this.flight_id = flight_id;
    }

    public String getAirlines_name() {
        return airlines_name;
    }

    public void setAirlines_name(String airlines_name) {
        this.airlines_name = airlines_name;
    }

    public String getFlight_number() {
        return flight_number;
    }

    public void setFlight_number(String flight_number) {
        this.flight_number = flight_number;
    }

    public String getPrice_value() {
        return price_value;
    }

    public void setPrice_value(String price_value) {
        this.price_value = price_value;
    }

    public String getFull_via() {
        return full_via;
    }

    public void setFull_via(String full_via) {
        this.full_via = full_via;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getStop_flight() {
        return stop_flight;
    }

    public void setStop_flight(String stop_flight) {
        this.stop_flight = stop_flight;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
