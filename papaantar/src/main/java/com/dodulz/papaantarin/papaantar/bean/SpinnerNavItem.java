package com.dodulz.papaantarin.papaantar.bean;

/**
 * Created by dodulz on 3/13/14 AD.
 */
public class SpinnerNavItem {

    private String title;
    private int icon;

    public SpinnerNavItem() {
    }

    public SpinnerNavItem(String title) {
        this.title = title;
    }

    public SpinnerNavItem(String title, int icon){
        this.title = title;
        this.icon = icon;
    }

    public String getTitle(){
        return this.title;
    }

    public int getIcon(){
        return this.icon;
    }
}