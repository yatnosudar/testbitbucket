package com.dodulz.papaantarin.papaantar.fragment;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dodulz.papaantarin.papaantar.R;

/**
 * Created by dodulz on 4/11/14.
 */
public class TiketDialogFragment extends DialogFragment
{

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_more,container,false);
        return view;
    }
}
