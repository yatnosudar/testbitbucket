package com.dodulz.papaantarin.papaantar.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.dodulz.papaantarin.papaantar.NewDesignActivity;
import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.VendorAgent;
import com.dodulz.papaantarin.papaantar.about.Agent;
import com.dodulz.papaantarin.papaantar.about.Tentang;
import com.dodulz.papaantarin.papaantar.adapter.AdapterRuteDetail;
import com.dodulz.papaantarin.papaantar.bean.Rutes;
import com.dodulz.papaantarin.papaantar.bean.RutesRepo;
import com.dodulz.papaantarin.papaantar.tiketcom.TIKETCOM_API;
import com.dodulz.papaantarin.papaantar.tiketcom.WebTiket;
import com.dodulz.papaantarin.papaantar.util.StaticVariable;

import java.util.List;

/**
 * Created by dodulz on 3/10/14 AD.
 */
public class RutesActivity extends ActionBarActivity {
    ListView listView;
    String slug = "";
    String travel = "";
    Button vendor,search,favorit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(0, 0);
        setContentView(R.layout.detail_rutes);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent zi = getIntent();
        int zix = zi.getIntExtra("status",1);

        vendor = (Button) findViewById(R.id.image_vendor);
        search = (Button) findViewById(R.id.image_search);
        favorit = (Button) findViewById(R.id.image_bookmark);

        if (zix==1){
            vendor.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.vendor_active), null, null);
            vendor.setTextColor(getResources().getColor(R.color.orange));
        }
        else if(zix==2){
            search.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.search_active), null, null);
            search.setTextColor(getResources().getColor(R.color.orange));
        }
        else if(zix==3){
            favorit.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.bookmark_active), null, null);
            favorit.setTextColor(getResources().getColor(R.color.orange));
        }

        slug = getIntent().getStringExtra("slug");
        travel = getIntent().getStringExtra("travel");
        setTitle(travel);
        listView = (ListView) findViewById(R.id.detail_rutes);
        AdapterRuteDetail adapterRuteDetail = new AdapterRuteDetail(RutesActivity.this,R.layout.item_key_val,GETDATA());
        listView.setAdapter(adapterRuteDetail);
    }

    public List<Rutes> GETDATA(){
        RutesRepo rutesRepo = new RutesRepo(RutesActivity.this);
        return rutesRepo.getAllWhere(slug);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
            case R.id.agent:
                startActivity(new Intent(RutesActivity.this,Agent.class));
                break;
            case R.id.tentang:
                startActivity(new Intent(RutesActivity.this,Tentang.class));
                break;
//            case R.id.action_search:
//                this.overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
//                startActivity(new Intent(RutesActivity.this,SearchActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
//                if (!new DetailActivity().isFinishing()){
//                    new DetailActivity().finish();
//                }
//                finish();
//                return true;
        }

        return true;
    }

    @Override
    public boolean navigateUpTo(Intent upIntent) {
        finish();
        return true;
    }

    public void _vendor(View v){
        Intent i = new Intent(RutesActivity.this, NewDesignActivity.class);
        i.putExtra("status",1);
        startActivity(i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        finish();
        overridePendingTransition(0, 0);
    }

    public void _cari(View v){
        Intent i = new Intent(RutesActivity.this, NewDesignActivity.class);
        i.putExtra("status",2);
        startActivity(i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        finish();
        overridePendingTransition(0, 0);
    }

    public void _bookmark(View v){
        Intent i = new Intent(RutesActivity.this, NewDesignActivity.class);
        i.putExtra("status",3);
        startActivity(i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        finish();
        overridePendingTransition(0, 0);
    }

    public void _more(View v){
//        Intent i = new Intent(RutesActivity.this, NewDesignActivity.class);
//        i.putExtra("status",4);
//        startActivity(i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
//        finish();
//        overridePendingTransition(0, 0);
        final Dialog dialog = new Dialog(RutesActivity.this,R.style.DialogAnimation);
        dialog.getWindow().setWindowAnimations(R.style.DialogAnimation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_more);
        Button btn1 = (Button) dialog.findViewById(R.id.btn1);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (StaticVariable.isOnline()){
                    if (TIKETCOM_API.TOKEN.length()==0){
                        TIKETCOM_API.token();
                    }
                    startActivity(new Intent(RutesActivity.this, WebTiket.class).putExtra("status", "10"));

                    //getActivity().startActivity(new Intent(getActivity(), TiketActivity.class).putExtra("spot","0"));
                }else{
                    Toast.makeText(RutesActivity.this, "Tidak ada jaringan...", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });
        Button btn2 = (Button) dialog.findViewById(R.id.btn2);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (StaticVariable.isOnline()){

                    startActivity(new Intent(RutesActivity.this, WebTiket.class).putExtra("status", "3"));
                }else{
                    Toast.makeText(RutesActivity.this,"Tidak ada jaringan...",Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });
        Button btn3 = (Button) dialog.findViewById(R.id.btn3);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (StaticVariable.isOnline()){
                    startActivity(new Intent(RutesActivity.this, WebTiket.class).putExtra("status", "2"));
                }else{
                    Toast.makeText(RutesActivity.this,"Tidak ada jaringan...",Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });
        Button btn4 = (Button) dialog.findViewById(R.id.btn4);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RutesActivity.this,VendorAgent.class));
                dialog.dismiss();
            }
        });
        Button btn5 = (Button) dialog.findViewById(R.id.btn5);
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RutesActivity.this,Tentang.class));
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
