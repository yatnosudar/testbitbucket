package com.dodulz.papaantarin.papaantar.about;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.dodulz.papaantarin.papaantar.R;

/**
 * Created by dodulz on 3/22/14.
 */
public class Tentang extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Tentang PapaAntar");
        setContentView(R.layout.tentang);
    }
}
