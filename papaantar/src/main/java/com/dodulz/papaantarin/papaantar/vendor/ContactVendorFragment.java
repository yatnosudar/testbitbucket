package com.dodulz.papaantarin.papaantar.vendor;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.bean.Contact;

/**
 * Created by dodulz on 4/11/14.
 */
public class ContactVendorFragment extends Fragment {

    private ListView listView;
    private AdapterContactAgentTravel adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_register_vendor,container,false);
        setHasOptionsMenu(true);
        ActionBar actionBar= ((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBar.setTitle("Contact");
        listView = (ListView) view.findViewById(R.id.list_vendor_register);
        adapter = new AdapterContactAgentTravel(getActivity(),R.layout.item_key_val,DataAgentRegister.AGENT_CONTACT);
        listView.setAdapter(adapter);
        listenerListview();
        return view;
    }

    public void listenerListview(){
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                Contact contact = DataAgentRegister.AGENT_CONTACT.get(i);
                final int position = i;
                builder.setTitle("Hapus data");
                builder.setMessage(
                        contact.getType_contact()+"\n"
                        +contact.getNo_contact()
                );
                builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        DataAgentRegister.AGENT_CONTACT.remove(position);
                        adapter.notifyDataSetChanged();
                        dialogInterface.dismiss();
                    }
                });

                builder.setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.show();
                return false;
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_submit_agent, menu);
        menu.findItem(R.id.action_submit_vendor).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                new BG_SEND_EMAIL(getActivity()).execute();
                return false;
            }
        });

        menu.findItem(R.id.action_back_rutes).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new RuteVendorFragment())
                        .commit();
                return false;
            }
        });

        menu.findItem(R.id.action_add_contact).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Tambah Contact");
                View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_contact,null);
                final EditText txt = (EditText)view.findViewById(R.id.id_address_contact);
                final Spinner tipe = (Spinner) view.findViewById(R.id.spinner_tipe_contact);
                builder.setView(view);
                builder.setPositiveButton("Tambah",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Contact contact = new Contact();
                        contact.setNo_contact(txt.getText().toString());
                        contact.setType_contact(tipe.getSelectedItem().toString());
//                        adapter.add(contact);
                        adapter.notifyDataSetChanged();
                        DataAgentRegister.AGENT_CONTACT.add(contact);
                        dialogInterface.dismiss();
                    }
                });
                builder.setNegativeButton("Batal",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.show();
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }


}
