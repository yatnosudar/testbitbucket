package com.dodulz.papaantarin.papaantar.bean;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dodulz on 3/5/14 AD.
 */
@DatabaseTable(tableName = "Contact")
public class Contact {

    @DatabaseField(generatedId = true)
    public int id;

    @DatabaseField
    public String slug_name;

    @DatabaseField
    public String no_contact;

    @DatabaseField
    public String type_contact;

    @DatabaseField
    public int status_query;

    public Contact() {
    }

    public Contact(String slug_name, String no_contact,String type_contact) {
        this.slug_name = slug_name;
        this.no_contact = no_contact;
        this.type_contact =type_contact;
    }

    public int getStatus_query() {
        return status_query;
    }

    public void setStatus_query(int status_query) {
        this.status_query = status_query;
    }

    public String getType_contact() {
        return type_contact;
    }

    public void setType_contact(String type_contact) {
        this.type_contact = type_contact;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSlug_name() {
        return slug_name;
    }

    public void setSlug_name(String slug_name) {
        this.slug_name = slug_name;
    }

    public String getNo_contact() {
        return no_contact;
    }

    public void setNo_contact(String no_contact) {
        this.no_contact = no_contact;
    }
}
