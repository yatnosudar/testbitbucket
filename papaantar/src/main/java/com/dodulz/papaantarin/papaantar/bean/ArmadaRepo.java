package com.dodulz.papaantarin.papaantar.bean;

import android.content.Context;
import android.util.Log;

import com.dodulz.papaantarin.papaantar.util.DBHelp;
import com.dodulz.papaantarin.papaantar.util.DBManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by dodulz on 3/10/14 AD.
 */
public class ArmadaRepo {

    public DBHelp db;
    Dao<Armada,Integer> questionDao;

    public ArmadaRepo(Context context) {
        try {
            DBManager dbManager = new DBManager();

            db = dbManager.getDbHelp(context);
            questionDao = db.getArmadasDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int create(Armada mediaModel){
        try {
            Log.e(Armada.class.toString(), "Success Save");
            return questionDao.create(mediaModel);
        }catch (SQLException e){
            Log.e(ArmadaRepo.class.toString(), e.getMessage());
        }
        return 0;
    }

    public int delete_all(int i){
        try {
            return questionDao.delete(getAll(i));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public int update(Armada mediaModel){
        try {
            return questionDao.delete(mediaModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public List getAll(int i){
        try {
            return questionDao.queryBuilder().where().eq("status_query",i).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public List getAllWhere(String slug,int query_type){
        try {
            return questionDao.queryBuilder().where().eq("slug",slug).and().eq("status_query",query_type).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List getAllLimit(){
        try {
            return questionDao.queryBuilder().limit(10).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
