package com.dodulz.papaantarin.papaantar;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.dodulz.papaantarin.papaantar.tiketcom.TIKETCOM_API;
import com.dodulz.papaantarin.papaantar.util.AsyncSAVEComponent;
import com.dodulz.papaantarin.papaantar.util.StaticVariable;

/**
 * Created by dodulz on 3/5/14 AD.
 */
public class SplashScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        new bg_process().execute();
//        new BG_SEND_EMAIL(SplashScreen.this).execute();
    }

    class bg_process extends AsyncTask<Void,Void,Boolean>{
        @Override
        protected Boolean doInBackground(Void... voids) {
            new AsyncSAVEComponent(SplashScreen.this).execute();
            TIKETCOM_API.token();
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(2000);
                        startActivity(new Intent(SplashScreen.this,NewDesignActivity.class));
                        finish();
                    } catch (InterruptedException e) {
                        StaticVariable.STATE_PROCESS = 2;
                        Log.d("Error Thread",e.getMessage());
                    }
                }
            }).start();
        }
    }
}
