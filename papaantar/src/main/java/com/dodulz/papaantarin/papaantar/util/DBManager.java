package com.dodulz.papaantarin.papaantar.util;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;

/**
 * Created by dodulz on 2/25/14 AD.
 */
public class DBManager {
    private DBHelp dbHelp = null;

    public DBHelp getDbHelp(Context context) {
        if (dbHelp==null){
            dbHelp = OpenHelperManager.getHelper(context, DBHelp.class);
        }
        return dbHelp;
    }
    public void releaseHelper(DBHelp helper)
    {
        if (helper != null) {
            OpenHelperManager.releaseHelper();
            dbHelp = null;
        }
    }
}
