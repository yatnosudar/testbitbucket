package com.dodulz.papaantarin.papaantar.bean;

import android.content.Context;
import android.util.Log;

import com.dodulz.papaantarin.papaantar.util.*;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by dodulz on 2/26/14 AD.
 */
public class CityRepo {
    public DBHelp db;
    Dao<City,Integer> questionDao;

    public CityRepo(Context context) {
        try {
            DBManager dbManager = new DBManager();

            db = dbManager.getDbHelp(context);
            questionDao = db.getCitysDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int create(City mediaModel){
        try {
            return questionDao.create(mediaModel);
        }catch (SQLException e){
            Log.e(CityRepo.class.toString(), e.getMessage());
        }
        return 0;
    }

    public int delete_all(){
        try {
            return questionDao.delete(getAll());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public int update(City mediaModel){
        try {
            return questionDao.delete(mediaModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List getAll(){
        try {
            return questionDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List getAllLimit(){
        try {
            return questionDao.queryBuilder().limit(10).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
