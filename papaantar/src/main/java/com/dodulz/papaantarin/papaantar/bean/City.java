package com.dodulz.papaantarin.papaantar.bean;

/**
 * Created by dodulz on 3/5/14 AD.
 */
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Jadwal")
public class City {

    @DatabaseField(generatedId = true)
    public int id;

    @DatabaseField
    public String name;

    @DatabaseField
    public String slug;

    @DatabaseField
    public String state;

    @DatabaseField
    public String slug_state;

    public City() {
    }

    public City(String name, String slug,String state,String slug_state) {
        this.name = name;
        this.slug = slug;
        this.state = state;
        this.slug_state = slug_state;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSlug_state() {
        return slug_state;
    }

    public void setSlug_state(String slug_state) {
        this.slug_state = slug_state;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
