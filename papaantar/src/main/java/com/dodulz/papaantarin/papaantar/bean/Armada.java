package com.dodulz.papaantarin.papaantar.bean;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dodulz on 3/10/14 AD.
 */
@DatabaseTable(tableName = "armada")
public class Armada {
    @DatabaseField(generatedId = true)
    public int id;

    @DatabaseField
    public String slug;

    @DatabaseField
    public String armada;

    @DatabaseField
    public String total;

    @DatabaseField
    public int status_query;

    public Armada() {
    }

    public Armada(String slug, String armada) {
        this.slug = slug;
        this.armada = armada;
    }

    public Armada(String slug, String armada, String total) {
        this.slug = slug;
        this.armada = armada;
        this.total = total;
    }

    public int getStatus_query() {
        return status_query;
    }

    public void setStatus_query(int status_query) {
        this.status_query = status_query;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getArmada() {
        return armada;
    }

    public void setArmada(String armada) {
        this.armada = armada;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
