package com.dodulz.papaantarin.papaantar.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.SearchActivity;
import com.dodulz.papaantarin.papaantar.adapter.AdapterResultSearchPlace;
import com.dodulz.papaantarin.papaantar.bean.Travel;
import com.dodulz.papaantarin.papaantar.bean.TravelRepo;

import java.util.List;

/**
 * Created by dodulz on 3/9/14 AD.
 */
public class BookmarkFragment extends Fragment{

    private ListView listView ;
    private AdapterResultSearchPlace adapter;
    private MenuItem searchItem ;
    private SearchView searchView ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SearchActivity.FROM_SEARCH_FRAGMENT = 0;
        View view = inflater.inflate(R.layout.main_result_search, container, false);
        setHasOptionsMenu(true);

        ActionBar actionBar= ((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBar.setTitle("Favorit");
        listView = (ListView) view.findViewById(R.id.list_search);
        List<Travel> a = GETDATA();
        adapter = new AdapterResultSearchPlace(getActivity(),R.layout.item_result_search,a);
        listView.setAdapter(adapter);
        listView.setTextFilterEnabled(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String slugname = GETDATA().get(i).getSlug_name();
                String travel = GETDATA().get(i).getName();
                String city = GETDATA().get(i).getCity();
                String type_travel = GETDATA().get(i).getType();
                String state = GETDATA().get(i).getState();
                String country = GETDATA().get(i).getCountry();
                String facilities = GETDATA().get(i).getFacilities();
                String address = GETDATA().get(i).getAddress();
                String logo = GETDATA().get(i).getUri_image();


                Intent ix = new Intent(getActivity(),DetailActivity.class);
                ix.putExtra("slug_name",slugname);
                ix.putExtra("travel",travel);
                ix.putExtra("city",city);
                ix.putExtra("type_travel",type_travel);
                ix.putExtra("state",state);
                ix.putExtra("country",country);
                ix.putExtra("facilities",facilities);
                ix.putExtra("address",address);
                ix.putExtra("logo",logo);
                ix.putExtra("status",3);
                ix.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(ix);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Travel t = adapter.getItem(i);
                new AlertDialogCall(getActivity(),t.getSlug_name(),1).show();
                return false;
            }
        });
        return view;
    }
    public List<Travel> GETDATA(){
        List<Travel> dt = new TravelRepo(getActivity()).getAll(2);
        return dt;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_place_fragment, menu);
        searchItem = menu.findItem(R.id.action_search_place);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Log.d("on Submit", s);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Log.d("querytext",s);
                if (adapter!=null){
                    adapter.getFilter().filter(s);
                }

                return true;
            }
        });



//        menu.findItem(R.id.agentx).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem menuItem) {
//                StaticVariable.STATE_PROCESS=2;
//                startActivity(new Intent(getActivity(),Agent.class));
//                return false;
//            }
//        });
//
//        menu.findItem(R.id.tentangx).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem menuItem) {
//                StaticVariable.STATE_PROCESS=2;
//                startActivity(new Intent(getActivity(),Tentang.class));
//                return false;
//            }
//        });
        super.onCreateOptionsMenu(menu, inflater);
    }
}
