package com.dodulz.papaantarin.papaantar.tiketcom;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.dodulz.papaantarin.papaantar.NewDesignActivity;
import com.dodulz.papaantarin.papaantar.R;

/**
 * Created by dodulz on 3/21/14.
 */
public class WebTiket extends ActionBarActivity {
    WebView wv;
    ProgressBar pg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.anim_in,R.anim.anim_out);
        setContentView(R.layout.web_tiket);
        wv = (WebView) findViewById(R.id.web_tiket);
        pg = (ProgressBar) findViewById(R.id.web_load_tiket);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.setWebViewClient(new SwAWebClient());

        String url = "";

        if(getIntent().getExtras().getString("status").equals("1"))
        {
            url = "http://m.tiket.com/order/add/flight?"
                    +"flight="
                    +getIntent().getExtras().getString("flight")
                    +"&date="
                    +getIntent().getExtras().getString("date")
                    +"&twh=19679332";
        }else if(getIntent().getExtras().getString("status").equals("0")){
             Log.d("panjang data",ResultFlightFragment.objectMap.size()+" = "+ResultFlightFragment.objectMap.toString());
             url = "http://m.tiket.com/order/add/flight?"
                    +"flight="
                    +ResultFlightFragment.objectMap.get("flight").toString()
                    +"&date="
                    +ResultFlightFragment.objectMap.get("date").toString()
                    +"ret_flight="
                    +ResultFlightFragment.objectMap.get("ret_flight").toString()
                    +"&ret_date="
                    +ResultFlightFragment.objectMap.get("ret_date").toString()
                    +"&twh=19679332";
        }else if (getIntent().getExtras().getString("status").equals("2")){
            setTitle("Cari Hotel");
            url = "http://m.tiket.com/hotel?twh=19679332";
        }else if (getIntent().getExtras().getString("status").equals("10")){
            setTitle("Cari Penerbangan");
            url = "http://m.tiket.com/pesawat?twh=19679332";
        }
        else{
            setTitle("Cari Kereta Api");
            url = "http://m.tiket.com/kereta-api?twh=19679332";
        }
        Log.d(url,url);
        wv.loadUrl(url);

    }

    private class SwAWebClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            pg.setVisibility(View.GONE);
            wv.setVisibility(View.VISIBLE);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            pg.setVisibility(View.GONE);
            wv.setVisibility(View.GONE);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(WebTiket.this, NewDesignActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        finish();
    }
}
