package com.dodulz.papaantarin.papaantar.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.dodulz.papaantarin.papaantar.bean.Rutes;
import com.dodulz.papaantarin.papaantar.bean.RutesRepo;
import com.dodulz.papaantarin.papaantar.fragment.RutesActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

/**
 * Created by dodulz on 3/10/14 AD.
 */
public class AsyncDETAILTravel extends AsyncTask<Void,Void,Boolean> {
    ProgressDialog progressDialog;
    Context context;
    String slug = "";
    String travel = "";
    int st;
    boolean status = false;
    Rutes rutes ;
    RutesRepo rutesRepo ;
    public AsyncDETAILTravel(Context context,String slug,String travel,int st){
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Memuat data...");
        rutes = new Rutes();
        rutesRepo = new RutesRepo(context);
        this.context = context;
        this.slug = slug;
        this.travel = travel;
        this.st = st;
    }

    public boolean prosess_data() throws IOException {
        ServiceHandler serviceHandler = new ServiceHandler();
        String res = serviceHandler.HTTP_GET(StaticVariable.BYSLUG+slug);
        Log.i("response",StaticVariable.BYSLUG+slug);
        try {
            JSONArray response =  new JSONArray(res);
            JSONObject jsonObject = response.getJSONObject(0);
            JSONArray jsonArray = jsonObject.getJSONArray("rutes");
            rutesRepo.delete_all(slug);
            for (int i = 0;i<jsonArray.length();i++){
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                rutes.setPrice(jsonObject1.getString("price"));
                rutes.setSlug(slug);
                rutes.setCar(jsonObject1.getString("car"));
                //save place
                JSONArray jsonArray1 = jsonObject1.getJSONArray("places");
                StringBuffer stringBuffer = new StringBuffer();
                for (int j=0;j<jsonArray1.length();j++){
                    stringBuffer.append(jsonArray1.getJSONObject(j).getString("name"));
                    if (j<jsonArray1.length()-1){
                        stringBuffer.append(" - ");
                    }
                }
                rutes.setPlace(stringBuffer.toString());
                rutesRepo.create(rutes);
            }
            status = true;
        } catch (JSONException e) {
            status = false;
            Log.e("json error",e.getMessage());
        }
        return status;
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog.show();
    }
    @Override
    protected Boolean doInBackground(Void... voids) {
        List<Rutes> rutesList = rutesRepo.getAllWhere(slug);
        if (rutesList.size()>0){
            return true;
        }else{
            try {
                return prosess_data();
            }catch (IOException e){
                return false;
            }
        }
    }
    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        progressDialog.dismiss();
        if (aBoolean){
            Intent intent = new Intent(context,RutesActivity.class);
            intent.putExtra("slug",slug);
            intent.putExtra("travel",travel);
            intent.putExtra("status",st);
            context.startActivity(intent);
        }
    }
}
