package com.dodulz.papaantarin.papaantar.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.bean.Flight;
import com.loopj.android.image.SmartImageView;

import java.util.List;

/**
 * Created by dodulz on 3/21/14.
 */
public class AdapterFlightSearch extends ArrayAdapter<Flight> {

    public Activity context;
    public int textViewResourceId = 0;
    public List<Flight> objects;

    public AdapterFlightSearch(Activity context,int textViewResourceId, List<Flight> objects) {
        super(context,textViewResourceId, objects);
        this.context = context;
        this.textViewResourceId = textViewResourceId;
        this.objects = objects;
    }

    static class ViewHolder{
        public TextView price;
        public TextView flight;
        public TextView code_flight;
        public TextView transit;
        public TextView time_transit;
        public SmartImageView imageView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rootView = convertView;
        if (rootView == null){
            LayoutInflater inflater = context.getLayoutInflater();
            rootView = inflater.inflate(textViewResourceId,null);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.price = (TextView) rootView.findViewById(R.id.price_flight);
            viewHolder.flight = (TextView) rootView.findViewById(R.id.airline);
            viewHolder.code_flight = (TextView) rootView.findViewById(R.id.code_flight);
            viewHolder.transit = (TextView) rootView.findViewById(R.id.transit);
            viewHolder.time_transit = (TextView) rootView.findViewById(R.id.time_transit);
            viewHolder.imageView = (SmartImageView) rootView.findViewById(R.id.image_airline);
            rootView.setTag(viewHolder);
        }else{
            rootView = convertView;
        }

        ViewHolder holder = (ViewHolder) rootView.getTag();
//        "Rp. "+harga.format("%,d", Integer.parseInt(harga))
        String harga = objects.get(position).getPrice_value();
        holder.price.setText("IDR "+harga.format("%,d", (int) Double.parseDouble(harga)));
        holder.flight.setText(objects.get(position).getAirlines_name());
        holder.code_flight.setText(objects.get(position).getFlight_number());
        holder.transit.setText(objects.get(position).getStop_flight());
        holder.time_transit.setText(objects.get(position).getFull_via().replace(", ","\n"));
        holder.imageView.setImageUrl(objects.get(position).getImage_url());

        return rootView;
    }
}
