package com.dodulz.papaantarin.papaantar.tiketcom;

import android.util.Log;

import com.dodulz.papaantarin.papaantar.util.MyApp;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dodulz on 3/20/14.
 */
public class TIKETCOM_API {
    public static String KEY_TIKET = "c1cd976c2bf71e6754de5c61a7cb9909";
    public static String BASE_URL_TOKEN = "https://api.master18.tiket.com/";
    public static String BASE_URL= "http://api.master18.tiket.com/";
    public static String TOKEN = "";

    public static String GET_TOKEN = BASE_URL_TOKEN+"apiv1/payexpress";
    public static String GET_FLIGHT = BASE_URL+"search/flight";
    public static RequestParams REQUEST_PARAM = null;
    public static Map<String,String> DATA_FLIGHT = new HashMap<String, String>();

    public static void FLIGHT_PARAM(String d,String a,String date,String ret_date,String adult,String child,String infant){
        RequestParams requestParams = new RequestParams();
        requestParams.add("d",d);
        requestParams.add("a",a);
        requestParams.add("date",date);
        if (ret_date.trim().length()>0 || ret_date!=null || !ret_date.equals("")){
            requestParams.add("ret_date",ret_date);
        }
        requestParams.add("adult",adult);
        requestParams.add("child",child);
        requestParams.add("infant",infant);
        requestParams.add("token",TOKEN);
        requestParams.add("v","3");
        requestParams.add("output","json");
        Log.d("request param",requestParams.toString());
        REQUEST_PARAM = requestParams;
    }


    public static AsyncHttpClient client = new AsyncHttpClient();


    public static void token(){
        /* GET TOKEN */
        RequestParams requestParams = new RequestParams();
        requestParams.add("method","getToken");
        requestParams.add("secretkey",TIKETCOM_API.KEY_TIKET);
        requestParams.add("output", "json");

        TIKETCOM_API.client.get(MyApp.getInstance(),TIKETCOM_API.GET_TOKEN,requestParams,new AsyncHttpResponseHandler(){
            @Override
            public void onSuccess(String content) {
                Log.d("TOKEN","ini token"+content);
                try {
                    JSONObject jsonObject = new JSONObject(content);
                    TIKETCOM_API.TOKEN = jsonObject.getString("token");
                    Log.d("TOKEN","ini token"+TIKETCOM_API.TOKEN);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
            /* GET TOKEN */
    }

}
