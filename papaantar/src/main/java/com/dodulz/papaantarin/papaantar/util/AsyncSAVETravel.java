package com.dodulz.papaantarin.papaantar.util;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.dodulz.papaantarin.papaantar.bean.Armada;
import com.dodulz.papaantarin.papaantar.bean.ArmadaRepo;
import com.dodulz.papaantarin.papaantar.bean.Contact;
import com.dodulz.papaantarin.papaantar.bean.ContactRepo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by dodulz on 3/11/14 AD.
 */
public class AsyncSAVETravel extends AsyncTask<Void,Void,Boolean> {

    JSONArray jsonContact[];
    JSONArray jsonArmada[];
    String slug[];
    Contact contact;
    Armada armada;
    ContactRepo contactRepo;
    ArmadaRepo armadaRepo;

    Context context;

    public AsyncSAVETravel(Context context,JSONArray jsonContact[], JSONArray jsonArmada[], String slug[]){
        this.jsonContact = jsonContact;
        this.jsonArmada = jsonArmada;
        this.slug = slug;
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        contact = new Contact();
        armada = new Armada();
        contactRepo = new ContactRepo(context);
        armadaRepo = new ArmadaRepo(context);
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        contactRepo.delete_all(1);
        armadaRepo.delete_all(1);

        StaticVariable staticVariable = new StaticVariable();
        for (int i = 0; i<jsonContact.length;i++){
            final int finalI = i;
            // contact
            staticVariable.save_data(new ProcessAsync() {
            @Override
            public void processData() {
                try {
                    JSONArray jsonArrayContact = jsonContact[finalI];
                    if (jsonArrayContact.length()>0){
                        for (int x =0; x<jsonArrayContact.length();x++){
                            JSONObject jsonObject = jsonArrayContact.getJSONObject(x);
                            contact.setSlug_name(slug[finalI]);
                            contact.setType_contact(jsonObject.getString("type"));
                            contact.setNo_contact(jsonObject.getString("address"));
                            contact.setStatus_query(1);
                            int iz = contactRepo.create(contact);
                        }
                    }
                } catch (JSONException e) {
                    Log.e("JSON ERROR", e.getMessage());
                }
            }
        });

            //armada
            staticVariable.save_data(new ProcessAsync() {
                @Override
                public void processData() {
                    try {
                        JSONArray jsonArrayArmada = jsonArmada[finalI];
                        if (jsonArrayArmada.length()>0){
                            for (int x =0;x<jsonArrayArmada.length();x++){
                                JSONObject jsonObject2 = jsonArrayArmada.getJSONObject(x);
                                armada.setArmada(jsonObject2.getString("merk"));
                                armada.setSlug(slug[finalI]);
                                armada.setTotal(jsonObject2.getString("total"));
                                armada.setStatus_query(1);
                                int iz = armadaRepo.create(armada);
                            }
                        }
                    } catch (JSONException e) {
                        Log.e("JSON ERROR", e.getMessage());
                    }
                }
            });
        }


        return null;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        Log.i("Status Save","FINISH SAVE ARMADA & CONTACT");
    }
}
