package com.dodulz.papaantarin.papaantar.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.tiketcom.TIKETCOM_API;
import com.dodulz.papaantarin.papaantar.tiketcom.WebTiket;
import com.dodulz.papaantarin.papaantar.util.StaticVariable;

/**
 * Created by dodulz on 3/14/14 AD.
 */
public class TiketFragment extends Fragment {

    ImageButton pesawat;
    ImageButton hotel;
    ImageButton kereta;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.all_tiket,container,false);

        ActionBar actionBar= ((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBar.setTitle("Tiket");
        pesawat = (ImageButton) v.findViewById(R.id.pesawat_img);
        hotel = (ImageButton) v.findViewById(R.id.hotel_img);
        kereta = (ImageButton) v.findViewById(R.id.kereta_img);

        pesawat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (StaticVariable.isOnline()){
                    if (TIKETCOM_API.TOKEN.length()==0){
                        TIKETCOM_API.token();
                    }
                    getActivity().startActivity(new Intent(getActivity(), WebTiket.class).putExtra("status","10"));
                    //getActivity().startActivity(new Intent(getActivity(), TiketActivity.class).putExtra("spot","0"));
                }else{
                    Toast.makeText(getActivity(),"Tidak ada jaringan...",Toast.LENGTH_SHORT).show();
                }
            }
        });

        hotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (StaticVariable.isOnline()){
                    getActivity().startActivity(new Intent(getActivity(), WebTiket.class).putExtra("status","2"));
                }else{
                    Toast.makeText(getActivity(),"Tidak ada jaringan...",Toast.LENGTH_SHORT).show();
                }

            }
        });

        kereta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (StaticVariable.isOnline()){
                    getActivity().startActivity(new Intent(getActivity(), WebTiket.class).putExtra("status","3"));
                }else{
                    Toast.makeText(getActivity(),"Tidak ada jaringan...",Toast.LENGTH_SHORT).show();
                }

            }
        });
        return v;
    }


}
