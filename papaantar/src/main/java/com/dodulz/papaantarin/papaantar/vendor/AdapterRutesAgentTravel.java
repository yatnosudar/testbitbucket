package com.dodulz.papaantarin.papaantar.vendor;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.bean.RutesAgent;

import java.util.List;

/**
 * Created by dodulz on 4/10/14.
 */
public class AdapterRutesAgentTravel extends ArrayAdapter<RutesAgent> {

    private Activity context;
    private int resource = 0;
    private List<RutesAgent> objects;

    public AdapterRutesAgentTravel(Activity context, int resource, List<RutesAgent> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }

    static class ViewHolder{
        TextView rutes;
        TextView harga;
        TextView tipe;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view  = null;
        if (view ==null){
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(resource,null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.harga = (TextView) view.findViewById(R.id.id_harga);
            viewHolder.rutes = (TextView)view.findViewById(R.id.id_rutes);
            viewHolder.tipe = (TextView)view.findViewById(R.id.id_tipe);
            view.setTag(viewHolder);
        }else{
            view = convertView;
        }
        ViewHolder vi = (ViewHolder)view.getTag();
        RutesAgent ra = objects.get(position);
        String harga = ra.getHarga();
        StringBuffer bf = new StringBuffer();
        if (ra.getTujuan1().length()>0){
            bf.append(" - "+ra.getTujuan1());
        }
        if (ra.getTujuan2().length()>0){
            bf.append(" - "+ra.getTujuan2());
        }
        if (ra.getTujuan3().length()>0){
            bf.append(" - "+ra.getTujuan3());
        }
        vi.rutes.setText(ra.getAsal()+bf.toString());
        vi.harga.setText("Rp. "+harga.format("%,d", Integer.parseInt(harga)));
        vi.tipe.setText(ra.getTipe());

        return view;
    }
}
