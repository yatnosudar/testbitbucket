package com.dodulz.papaantarin.papaantar.tiketcom;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.bean.AirportRepo;
import com.dodulz.papaantarin.papaantar.util.StaticVariable;

import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by dodulz on 3/20/14.
 */
public class SearchFlightFragment extends Fragment {
    AutoCompleteTextView berangkat;
    AutoCompleteTextView tiba;
    Button btn;
    TextView pergi;
    TextView pulang, textView6;
    CheckBox aSwitch;
    Spinner dewasa, anak, bayi;

    Calendar c = Calendar.getInstance();
    int x = 0, y = 0, z = 0;
    int startYear = c.get(Calendar.YEAR);
    int startMonth = c.get(Calendar.MONTH);
    int startDay = c.get(Calendar.DAY_OF_MONTH);


    class StartDatePicker extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        public int STATE = 0;

        StartDatePicker(int STATE) {
            this.STATE = STATE;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, startYear, startMonth, startDay);
            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            // Do something with the date chosen by the user
            x = year;
            y = monthOfYear;
            z = dayOfMonth;

            if (STATE == 0) {
                int s = y + 1;
                pergi.setText(x + "-" + s + "-" + z);
            } else {
                int s = y + 1;
                pulang.setText(x + "-" + s + "-" + z);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.search_flight, container, false);
        getActivity().setTitle("Cari Penerbangan");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, StaticVariable.AIRPORT);

        berangkat = (AutoCompleteTextView) view.findViewById(R.id.berangkat_flight);
        tiba = (AutoCompleteTextView) view.findViewById(R.id.tiba_flight);

        pergi = (TextView) view.findViewById(R.id.start_date_flight);
        pulang = (TextView) view.findViewById(R.id.end_date_flight);


        aSwitch = (CheckBox) view.findViewById(R.id.checkBox);

        berangkat.setAdapter(adapter);
        tiba.setAdapter(adapter);
        btn = (Button) view.findViewById(R.id.button_cari_flight);

        textView6 = (TextView) view.findViewById(R.id.textView6);

        dewasa = (Spinner) view.findViewById(R.id.adult);
        anak = (Spinner) view.findViewById(R.id.child);
        bayi = (Spinner) view.findViewById(R.id.infant);

        listener();

        return view;
    }

    public void listener() {

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    pulang.setVisibility(View.VISIBLE);
                    textView6.setVisibility(View.VISIBLE);
                    pulang.setText("Pilih Tanggal Kembali");
                } else {
                    pulang.setText("");
                    pulang.setVisibility(View.GONE);
                    textView6.setVisibility(View.GONE);
                }
            }
        });
        pergi.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                DialogFragment dialogFragment = new StartDatePicker(0);
                dialogFragment.show(getFragmentManager(), "start_date_picker");
            }
        });

        pulang.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                DialogFragment dialogFragment = new StartDatePicker(1);
                dialogFragment.show(getFragmentManager(), "start_date_picker");
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TIKETCOM_API.DATA_FLIGHT = new HashMap<String, String>();
                AirportRepo airportRepo = new AirportRepo(getActivity());
                String code_pergi = airportRepo.getCode(berangkat.getText().toString());
                String code_pulang = airportRepo.getCode(tiba.getText().toString());

                if (TIKETCOM_API.TOKEN.length() > 0) {
                    if (berangkat.getText().length() > 0) {
                        if (tiba.getText().length() > 0) {
                            if (!pergi.getText().equals("Pilih Tanggal Berangkat")) {
                                if (!berangkat.getText().toString().equals(tiba.getText().toString())) {

                                    if (pulang.equals("Pilih Tanggal Kembali")) {
                                        pulang.setText("");
                                    }

                                    //String d,String a,String date,String ret_date,String adult,String child,String infant
                                    TIKETCOM_API.DATA_FLIGHT.put("d", code_pergi);
                                    TIKETCOM_API.DATA_FLIGHT.put("a", code_pulang);

                                    TIKETCOM_API.DATA_FLIGHT.put("da", berangkat.getText().toString());
                                    TIKETCOM_API.DATA_FLIGHT.put("aa", tiba.getText().toString());

                                    TIKETCOM_API.DATA_FLIGHT.put("date", pergi.getText().toString());
                                    TIKETCOM_API.DATA_FLIGHT.put("ret_date", pulang.getText().toString());

                                    TIKETCOM_API.DATA_FLIGHT.put("adult", dewasa.getSelectedItem().toString());
                                    TIKETCOM_API.DATA_FLIGHT.put("child", anak.getSelectedItem().toString());
                                    TIKETCOM_API.DATA_FLIGHT.put("infant", bayi.getSelectedItem().toString());

                                    TIKETCOM_API.FLIGHT_PARAM(
                                            code_pergi,
                                            code_pulang,
                                            pergi.getText().toString(),
                                            pulang.getText().toString(),
                                            dewasa.getSelectedItem().toString(),
                                            anak.getSelectedItem().toString(),
                                            bayi.getSelectedItem().toString()
                                    );

                                    getActivity().startActivity(new Intent(getActivity(), ResultFlightFragment.class));


                                } else {
                                    Toast.makeText(getActivity(), "Kota asal dan tujuan tidak boleh sama...", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), "Masukan tanggal keberangkatan...", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Masukan kota tujuan...", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Masukan kota asal...", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Terjadi kesalah, Coba lagi...", Toast.LENGTH_LONG).show();
                    TIKETCOM_API.token();
                }

            }
        });
    }


}
