package com.dodulz.papaantarin.papaantar.bean;

import android.content.Context;
import android.util.Log;

import com.dodulz.papaantarin.papaantar.util.DBHelp;
import com.dodulz.papaantarin.papaantar.util.DBManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by dodulz on 2/26/14 AD.
 */
public class ContactRepo {
    public DBHelp db;
    Dao<Contact,Integer> questionDao;

    public ContactRepo(Context context) {
        try {
            DBManager dbManager = new DBManager();

            db = dbManager.getDbHelp(context);
            questionDao = db.getContactsDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int create(Contact mediaModel){
        try {
            return questionDao.create(mediaModel);
        }catch (SQLException e){
            Log.e(ContactRepo.class.toString(), e.getMessage());
        }
        return 0;
    }

    public int delete_all(int i){
        try {
            return questionDao.delete(getAll(i));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public int update(Contact mediaModel){
        try {
            return questionDao.delete(mediaModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public List getAll(int i){
        try {
            return questionDao.queryBuilder().where().eq("status_query",i).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public List getAll(){
        try {
            return questionDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public List getAllWhere(String slug,int query_status){
        try {
            return questionDao.queryBuilder().where().eq("slug_name",slug).and().eq("status_query",query_status).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List getAllLimit(){
        try {
            return questionDao.queryBuilder().limit(10).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
