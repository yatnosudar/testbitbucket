package com.dodulz.papaantarin.papaantar.vendor;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.dodulz.papaantarin.papaantar.bean.Agent;
import com.dodulz.papaantarin.papaantar.bean.Contact;
import com.dodulz.papaantarin.papaantar.bean.RutesAgent;
import com.dodulz.papaantarin.papaantar.mail.GmailSender;
import com.dodulz.papaantarin.papaantar.util.StaticVariable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by dodulz on 4/11/14.
 */
public class BG_SEND_EMAIL extends AsyncTask<Void,Integer,Boolean> {

    Activity context;
    ProgressDialog dialog;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setMessage("Kirim pesan...");
        dialog.show();
    }

    public BG_SEND_EMAIL(Activity context) {
        this.context = context;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        StringBuffer STRING_RUTES = new StringBuffer();
        StringBuffer STRING_CONTACT = new StringBuffer();
        String STRING_RUTES_FILENAME = "rutes.csv";

        List<RutesAgent> rutesAgents =  DataAgentRegister.AGENT_RUTES;
        for (int i=0;i<rutesAgents.size();i++){
            RutesAgent ra = rutesAgents.get(i);
            STRING_RUTES.append(ra.getTipe()+","+ra.getHarga()+","+ra.getAsal()+","+ra.getTujuan1()+","+ra.getTujuan2()+","+ra.getTujuan3()+"\n");
        }
        FileOutputStream fos = null;
        try {
            fos = context.openFileOutput(STRING_RUTES_FILENAME, Context.MODE_PRIVATE);
            fos.write(STRING_RUTES.toString().getBytes());
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Agent a = DataAgentRegister.AGENT_BIODATA;
        List<Contact> contacts = DataAgentRegister.AGENT_CONTACT;
        for (int i=0;i<contacts.size();i++){
            Contact contact1 = contacts.get(i);
            STRING_CONTACT.append(contact1.getType_contact()+" : "+contact1.getNo_contact()+"\n");
        }
        try {
            File f =  new File(context.getFilesDir(),STRING_RUTES_FILENAME);
            GmailSender gmailSender = new GmailSender("yatno.dodulz@gmail.com","adeqomariyah1989adem2o");

            String pesan = "BERIKUT INI DATA DARI TRAVEL : "+ a.getNama()+"\n"+
                    "NAMA : "+a.getNama()+"\n"+
                    "ALAMAT : "+a.getAlamat()+"\n"+
                    "KOTA : "+a.getKota()+"\n"+
                    "PROVINSI : "+a.getProvinsi()+"\n"+
                    "TIPE : "+a.getTipe()+"\n"+
                    "FASILITAS : "+a.getFasilitas()+"\n"+
                    "DESKRIPSI : "+a.getDeskripsi()+"\n"+
                    "\n\n===================================\n\n"+
                    STRING_CONTACT.toString();
            Log.d("pesan",pesan);
            gmailSender.sendMail(
                    DataAgentRegister.AGENT_BIODATA.getNama(),
                    pesan,
                    "yatno.dodulz@gmail.com",
                    "support@papaantar.in",
                    f.getPath()
            );
            return true;
        } catch (Exception e) {
            Log.e("error_mail", e.getMessage());
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        dialog.dismiss();
        if (aBoolean){
            DataAgentRegister.AGENT_CONTACT.clear();
            DataAgentRegister.AGENT_RUTES.clear();
            DataAgentRegister.AGENT_BIODATA = null;
            DataAgentRegister.AGENT_BIODATA = new Agent();
            Toast.makeText(context,"Data Sudah Terkirim,\nData akan diproses 2x24 jam",2000).show();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(3000);
                        context.finish();
                    } catch (InterruptedException e) {
                        StaticVariable.STATE_PROCESS = 2;
                        Log.d("Error Thread",e.getMessage());
                    }
                }
            }).start();
        }else {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Terjadi kesalahan sistem, coba lagi");
            builder.setPositiveButton("Ya",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    new BG_SEND_EMAIL(context).execute();
                }
            });
            builder.setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    context.finish();
                }
            });
        }
        super.onPostExecute(aBoolean);
    }
}
