package com.dodulz.papaantarin.papaantar.util;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.dodulz.papaantarin.papaantar.bean.Airport;
import com.dodulz.papaantarin.papaantar.bean.AirportRepo;
import com.dodulz.papaantarin.papaantar.bean.City;
import com.dodulz.papaantarin.papaantar.bean.CityRepo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by dodulz on 3/20/14.
 */
public class AsyncSAVEComponent extends AsyncTask<Void,Void,Boolean> {

    Activity context;
    public AsyncSAVEComponent(Activity context) {
        this.context = context;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        StaticVariable.save_data(new ProcessAsync() {
            @Override
            public void processData() {
                List<Airport> airportList = new AirportRepo(context).getAll();
                if (airportList.size()==0){
                try {
                    String airports = StaticVariable.getStringFromFile(context, "airports.json");
                    Log.d("Load Json Airport", airports);
                    final JSONArray jsonObject = new JSONArray(airports);
                    final AirportRepo airportRepo = new AirportRepo(context);
                    for (int i = 0; i<jsonObject.length();i++){
                        final JSONObject object = jsonObject.getJSONObject(i);
                        StaticVariable.save_data(new ProcessAsync() {
                            @Override
                            public void processData() {
                                Airport airport = new Airport();
                                try {
                                    StaticVariable.AIRPORT.add(object.getString("location_name"));
                                    airport.setCity(object.getString("location_name"));
                                    airport.setCode(object.getString("airport_code"));
                                    airportRepo.create(airport);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                } catch (JSONException e) {
                    Log.d("status_download_cat", "true : "+e.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                }else {
                    if (StaticVariable.AIRPORT.size()<=0){
                        for (int i =0;i<airportList.size();i++){
                            StaticVariable.AIRPORT.add(airportList.get(i).getCity());
                        }
                    }
                }
            }
        });

        StaticVariable.save_data(new ProcessAsync() {
            @Override
            public void processData() {
                List<City> cities = new CityRepo(context).getAll();
                if (cities.size()==0){
                            try {
                                String content = StaticVariable.getStringFromFile(context, "place.json");
                                Log.d("Load Json Place", content);
                                JSONArray jsonArray = new JSONArray(content);
                                final CityRepo cityRepo = new CityRepo(context);
                                final City city = new City();
                                for (int i = 0; i<jsonArray.length();i++){
                                    final JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    StaticVariable.save_data(new ProcessAsync() {
                                        @Override
                                        public void processData() {
                                            try {
                                                city.setState(jsonObject.getString("state_name"));
                                                city.setName(jsonObject.getString("name"));
                                                city.setSlug(jsonObject.getString("slug"));
                                                city.setSlug_state(jsonObject.getString("state_slug"));
                                                cityRepo.create(city);

                                                StaticVariable.CITY.add(jsonObject.getString("name"));
                                            } catch (JSONException e) {
                                                Log.e("error add city",e.getMessage());
                                            }
                                        }
                                    });
                                }
                                Log.d("status_download_cat", "true");
                            } catch (JSONException e) {
                                Log.d("status_download_cat", "true : "+e.getMessage());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                }else {
                    if (StaticVariable.CITY.size()<=0){
                        for (int i =0;i<cities.size();i++){
                            StaticVariable.CITY.add(cities.get(i).getName());
                        }
                    }
                }
            }
        });
        return null;
    }
}
