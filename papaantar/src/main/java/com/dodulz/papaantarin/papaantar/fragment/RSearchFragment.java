package com.dodulz.papaantarin.papaantar.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SpinnerAdapter;

import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.SearchActivity;
import com.dodulz.papaantarin.papaantar.adapter.AdapterResultSearch;
import com.dodulz.papaantarin.papaantar.bean.SpinnerNavItem;
import com.dodulz.papaantarin.papaantar.bean.Travel;
import com.dodulz.papaantarin.papaantar.bean.TravelRepo;
import com.dodulz.papaantarin.papaantar.util.AsyncSAVETravel;
import com.dodulz.papaantarin.papaantar.util.StaticVariable;
import com.dodulz.papaantarin.papaantar.util.TitleNavigationAdapter;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dodulz on 3/9/14 AD.
 */
public class RSearchFragment extends Fragment implements ActionBar.OnNavigationListener{

    ListView listView ;
    LinearLayout pg_loader;
    AdapterResultSearch adapter;
    String query,berangkat,tiba,vendor = "";
    private SpinnerAdapter mSpinnerAdapter;
    public static int RESULT_POSITION = 0;
    TravelRepo travelRepo = new TravelRepo(getActivity());
    ActionBar actionBar;
    private ArrayList<SpinnerNavItem> navSpinner;
    private TitleNavigationAdapter adapterNav;

    public RSearchFragment(String berangkat, String tiba, String vendor){
        if (berangkat!=""&&tiba!=""){
            query = berangkat+" - "+tiba;
        }else{
            query = vendor;
        }
        this.berangkat = berangkat;
        this.tiba = tiba;
        this.vendor = vendor;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SearchActivity.FROM_SEARCH_FRAGMENT = 2;
        View view = inflater.inflate(R.layout.main_result_search_dua,container,false);


        hiddenKeyboard(view);


        travelRepo.delete_all(1);
        actionBar= ((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBar.setTitle(query);
        navSpinner = new ArrayList<SpinnerNavItem>();
        navSpinner.add(new SpinnerNavItem("ALL"));
        navSpinner.add(new SpinnerNavItem("SUTTLE CAR"));
        navSpinner.add(new SpinnerNavItem("RENT CAR"));
        navSpinner.add(new SpinnerNavItem("CARTER"));
        navSpinner.add(new SpinnerNavItem("SUTTLE BUS"));

        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        actionBar.setDisplayShowTitleEnabled(true);

//        mSpinnerAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.action_list,
//                android.R.layout.simple_spinner_dropdown_item);
        adapterNav = new TitleNavigationAdapter(getActivity(), navSpinner);

        listView = (ListView) view.findViewById(R.id.list_search_dua);
        pg_loader = (LinearLayout) view.findViewById(R.id.pg_load);

        adapter = new AdapterResultSearch(getActivity(),R.layout.item_result_search,GETDATA());
        listView.setAdapter(adapter);

        actionBar.setListNavigationCallbacks(adapterNav,this);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Travel t = adapter.getItem(i);
                String slugname = t.getSlug_name();
                String travel = t.getName();
                String city = t.getCity();
                String type_travel = t.getType();
                String state = t.getState();
                String country = t.getCountry();
                String facilities = t.getFacilities();
                String address = t.getAddress();


                Intent ix = new Intent(getActivity(),DetailActivity.class);
                ix.putExtra("slug_name",slugname);
                ix.putExtra("travel",travel);
                ix.putExtra("city",city);
                ix.putExtra("type_travel",type_travel);
                ix.putExtra("state",state);
                ix.putExtra("country",country);
                ix.putExtra("facilities",facilities);
                ix.putExtra("address",address);
                ix.putExtra("logo",t.getUri_image());
                ix.putExtra("status",2);
                ix.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(ix);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Travel t = adapter.getItem(i);
                new AlertDialogCall(getActivity(),t.getSlug_name(),1).show();
                return false;
            }
        });
        new BG_PROCESS_SEARCH().execute();
        return view;
    }

    @Override
    public void onPause() {
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

        super.onPause();
    }

    private void hiddenKeyboard(View v) {
        InputMethodManager keyboard = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        keyboard.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }


    public void updateList(int i){
        RESULT_POSITION = i;
        adapter.clear();
        adapter.notifyDataSetChanged();
        List<Travel> dt = GETDATA();
        for (int ix = 0; ix < dt.size();ix++){
            adapter.add(dt.get(ix));
        }
        adapter.notifyDataSetChanged();
    }

    public List<Travel> GETDATA(){
        List<Travel> dt =  travelRepo.getWhereType(RESULT_POSITION,1);
        if (RESULT_POSITION==0){
            dt = new TravelRepo(getActivity()).getAll(1);
        }
        return dt;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onNavigationItemSelected(int i, long l) {
        updateList(i);
        return false;
    }


    public class BG_PROCESS_SEARCH extends AsyncTask<Void,Void,Boolean>{


        @Override
        protected Boolean doInBackground(Void... voids) {
            String URLi = "";
            String query_uri = "";
            try {
                query_uri = URLEncoder.encode(berangkat, "utf-8")+"/"+URLEncoder.encode(tiba,"utf-8")+"/"+URLEncoder.encode(vendor,"utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            URLi = StaticVariable.ROUTE+ query_uri;
            final String finalURLi = URLi;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AsyncHttpClient ahc = new AsyncHttpClient();
                    ahc.get(getActivity(), finalURLi, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(String response) {

                        if (response.equals("{\"status\":\"no data\"}")){
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage("Rute belum tersedia...")
                                    .setPositiveButton("Cari Rute Lain", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            ResultSearchFragment.RESULT_POSITION = 0;
                                            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
                                            getActivity().getSupportFragmentManager().beginTransaction()
                                                    .replace(R.id.container_main, new SearchFragment())
                                                    .commit();
                                        }
                                    });
                            builder.setCancelable(false);
                            if (!getActivity().isFinishing()){
                                builder.show();
                            }

                        }else{
                            pg_loader.setVisibility(View.GONE);
                            listView.setVisibility(View.VISIBLE);
                        try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                Log.i("total travel", jsonArray.length() + "");
                                JSONArray contactArray[] = new JSONArray[jsonArray.length()];
                                JSONArray armadaArray[] = new JSONArray[jsonArray.length()];
                                String slugArray[] = new String[jsonArray.length()];

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    JSONArray jsonArray1 = jsonObject1.getJSONArray("contacts");
                                    Log.i("total contact travel " + i, jsonArray1.length() + "");

                                    JSONArray jsonArray2 = jsonObject1.getJSONArray("rutes");
                                    Log.i("total rutes travel " + i, jsonArray2.length() + "");
                                    Travel travel = new Travel();
                                    travel.setName(jsonObject1.getString("name"));
                                    travel.setSlug_name(jsonObject1.getString("slug"));
                                    travel.setCity(jsonObject1.getString("city"));
                                    travel.setHarga(jsonArray2.getJSONObject(0).getString("price"));
                                    travel.setType(jsonObject1.getString("type"));
                                    travel.setAddress(jsonObject1.getString("address"));
                                    travel.setCountry(jsonObject1.getString("country"));
                                    travel.setFacilities(jsonObject1.getString("facilities"));
                                    travel.setState(jsonObject1.getString("state"));
                                    travel.setStatus_query(1);
                                    travel.setUri_image(jsonObject1.getString("logo"));

                                    travelRepo.create(travel);
                                    adapter.add(travel);

                                    contactArray[i] = jsonArray1;
                                    armadaArray[i] = jsonObject1.getJSONArray("armadas");
                                    slugArray[i] = jsonObject1.getString("slug");
                                }
                                new AsyncSAVETravel(getActivity(), contactArray, armadaArray, slugArray).execute();
                            } catch (JSONException e) {
                                Log.e("JSON ERROR ", e.getMessage());
                            }
                        }
                        }

                        @Override
                        public void onFailure(Throwable error) {
                            Log.e("ERROR GET DATA", error.toString());
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                            builder.setMessage("Jaringan terputus dari client atau dari server... ")
                                    .setPositiveButton("Kembali", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            ResultSearchFragment.RESULT_POSITION = 0;
                                            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
                                            getActivity().getSupportFragmentManager().beginTransaction()
                                                    .replace(R.id.container_main, new SearchFragment())
                                                    .commit();
                                        }
                                    });
                            builder.setCancelable(false);
                            if (!getActivity().isFinishing()){
                                builder.show();
                            }
                        }

                    });

                }
            });
            return null;
        }
    }
}
