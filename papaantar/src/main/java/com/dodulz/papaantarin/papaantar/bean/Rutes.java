package com.dodulz.papaantarin.papaantar.bean;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dodulz on 3/10/14 AD.
 */
@DatabaseTable(tableName = "travelsave")
public class Rutes {

    @DatabaseField(generatedId = true)
    public int id;

    @DatabaseField
    public String slug;

    @DatabaseField
    public String price;

    @DatabaseField
    public String car;

    @DatabaseField
    public String place;

    public Rutes(String slug, String price, String car, String place) {
        this.slug = slug;
        this.price = price;
        this.car = car;
        this.place = place;
    }

    public Rutes() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }
}
