package com.dodulz.papaantarin.papaantar.bean;

import android.content.Context;
import android.util.Log;

import com.dodulz.papaantarin.papaantar.util.DBHelp;
import com.dodulz.papaantarin.papaantar.util.DBManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by dodulz on 2/26/14 AD.
 */
public class TravelRepo {
    public DBHelp db;
    Dao<Travel,Integer> questionDao;

    public TravelRepo(Context context) {
        try {
            DBManager dbManager = new DBManager();

            db = dbManager.getDbHelp(context);
            questionDao = db.getTravelsDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int create(Travel mediaModel){
        try {
            return questionDao.create(mediaModel);
        }catch (SQLException e){
            Log.e(TravelRepo.class.toString(), e.getMessage());
        }
        return 0;
    }

    public int delete_all(int i,String data){
        try {
            return questionDao.delete(getAll(i,data));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public int delete_all(int i){
        try {
            return questionDao.delete(getAll(i));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public List getWhereType(int type,int position){
        try {
            switch (type){
                case 1:
                    //1 5 6 7 11 12 13 14 15
                    return questionDao.queryBuilder()
                            .where().eq("type","1")
                            .or().eq("type","5")
                            .or().eq("type","6")
                            .or().eq("type","7")
                            .or().eq("type","11")
                            .or().eq("type","12")
                            .or().eq("type","13")
                            .or().eq("type","14")
                            .or().eq("type","15")
                            .and().eq("status_query",position).query();
//                    break;
                case 2:
                    // 2 7 9 12 13 15
                    return questionDao.queryBuilder()
                            .where().eq("type","2")
                            .or().eq("type","7")
                            .or().eq("type","9")
                            .or().eq("type","12")
                            .or().eq("type","13")
                            .or().eq("type","15")
                            .and().eq("status_query",position).query();
//                    break;
                case 3:
                    // 3 6 7 9 12 15
                    return questionDao.queryBuilder()
                            .where().eq("type","3")
                            .or().eq("type","6")
                            .or().eq("type","7")
                            .or().eq("type","12")
                            .or().eq("type","9")
                            .or().eq("type","13")
                            .or().eq("type","15")
                            .and().eq("status_query",position).query();
//                    break;
                case 4:
                    // 4 4 8 9 11 12 14 15
                    return questionDao.queryBuilder()
                            .where().eq("type","4")
                            .or().eq("type","8")
                            .or().eq("type","9")
                            .or().eq("type","12")
                            .or().eq("type","11")
                            .or().eq("type","14")
                            .or().eq("type","15")
                            .and().eq("status_query",position).query();
//                    break;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }
    public int update(Travel mediaModel){
        try {
            return questionDao.delete(mediaModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public List getAll(String slug){
        try {
            return questionDao.queryBuilder().where().eq("slug_name",slug).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public List getAll(){
        try {
            return questionDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List getAll(int i){
        try {
            return questionDao.queryBuilder().where().eq("status_query",i).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public List getAll(int i,String data){
        try {
            return questionDao.queryBuilder().where().eq("status_query",i).and().like("city",data).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public List getAllSlug(int i,String data){
        try {
            return questionDao.queryBuilder().where().eq("status_query",i).and().like("slug_name",data).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public List getAllLimit(){
        try {
            return questionDao.queryBuilder().limit(10).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
