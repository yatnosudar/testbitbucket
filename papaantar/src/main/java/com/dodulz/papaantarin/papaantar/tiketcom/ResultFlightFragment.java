package com.dodulz.papaantarin.papaantar.tiketcom;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dodulz.papaantarin.papaantar.MainActivity;
import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.adapter.AdapterFlightSearch;
import com.dodulz.papaantarin.papaantar.bean.Flight;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dodulz on 3/21/14.
 */
public class ResultFlightFragment extends ActionBarActivity {
    boolean mStopHandler = true;
    Handler mHandler = new Handler();
    Runnable runnable;
    LinearLayout progres_flight;
    ListView listView;
    TextView c1, c2, c3, c4, c5, c6, c7, c8, c9, c10;
    AdapterFlightSearch adapterFlightSearch;

    public static int STATE = 0;
    public List<Flight> flights = new ArrayList<Flight>();
    public static Map<String, Object> objectMap = new HashMap<String, Object>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.anim_in,R.anim.anim_out);
        setContentView(R.layout.result_flight);

        if (TIKETCOM_API.DATA_FLIGHT.get("ret_date").trim().length() > 0) {

        } else {
            setTitle("Hasil Pencarian");
        }
        progres_flight = (LinearLayout) findViewById(R.id.progres_flight);

        List<Flight> f = new ArrayList<Flight>();
        adapterFlightSearch = new AdapterFlightSearch(ResultFlightFragment.this, R.layout.item_flight, f);

        listView = (ListView) findViewById(R.id.result_flight);
        listView.setAdapter(adapterFlightSearch);

        init();

        runnable = new Runnable() {
            @Override
            public void run() {
                if (mStopHandler) {
                    new BG_RESULT_FLIGHT().execute();
                    mHandler.postDelayed(this, 10000);

                }
            }
        };

        mHandler.post(runnable);


        listener();
    }

    @Override
    protected void onPause() {
        super.onPause();
        setmStopHandler();
    }

    @Override
    protected void onStop() {
        super.onStop();
        setmStopHandler();
    }

    public void init() {

        c1 = (TextView) findViewById(R.id.c_1);
        c2 = (TextView) findViewById(R.id.c_2);
        c3 = (TextView) findViewById(R.id.c_3);
        c4 = (TextView) findViewById(R.id.c_4);
        c5 = (TextView) findViewById(R.id.c_5);
        c6 = (TextView) findViewById(R.id.c_6);
        c7 = (TextView) findViewById(R.id.c_7);
        c8 = (TextView) findViewById(R.id.c_8);
        c9 = (TextView) findViewById(R.id.c_9);
        c10 = (TextView) findViewById(R.id.c_10);

        c1.setText(TIKETCOM_API.DATA_FLIGHT.get("d"));
        c2.setText(TIKETCOM_API.DATA_FLIGHT.get("a"));
        c3.setText(TIKETCOM_API.DATA_FLIGHT.get("da"));
        c4.setText(TIKETCOM_API.DATA_FLIGHT.get("aa"));

        c5.setText(TIKETCOM_API.DATA_FLIGHT.get("date"));
        if (TIKETCOM_API.DATA_FLIGHT.get("ret_date").trim().length() > 0) {
            c6.setText(TIKETCOM_API.DATA_FLIGHT.get(" - "));
            c7.setText(TIKETCOM_API.DATA_FLIGHT.get("ret_date"));
        }
        c8.setText(TIKETCOM_API.DATA_FLIGHT.get("adult") + " dewasa");
        c9.setText(TIKETCOM_API.DATA_FLIGHT.get("child") + " anak");
        c10.setText(TIKETCOM_API.DATA_FLIGHT.get("infant") + " bayi");
    }

    public void listener() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Flight x = adapterFlightSearch.getItem(i);
                if (TIKETCOM_API.DATA_FLIGHT.get("ret_date").trim().length() > 0) {
                    objectMap.put("date", TIKETCOM_API.DATA_FLIGHT.get("date"));
                    objectMap.put("ret_date", TIKETCOM_API.DATA_FLIGHT.get("ret_date"));

                    if (STATE == 2) {
                        if (x.getFlight_id()!=null ||x.getFlight_id().trim().length()>0){
                            objectMap.put("flight", x.getFlight_id());

                            adapterFlightSearch.clear();
                            adapterFlightSearch.notifyDataSetChanged();
                            Intent o = new Intent(ResultFlightFragment.this, ResultFlightFragment.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(o);
                            finish();
                            STATE = 3;

                        }else{
                            Toast.makeText(ResultFlightFragment.this,"Tidak memiliki flight id",Toast.LENGTH_SHORT).show();
                        }
                        setmStopHandler();
                    } else {
                        c1.setText(TIKETCOM_API.DATA_FLIGHT.get("a"));
                        c2.setText(TIKETCOM_API.DATA_FLIGHT.get("d"));
                        c3.setText(TIKETCOM_API.DATA_FLIGHT.get("aa"));
                        c4.setText(TIKETCOM_API.DATA_FLIGHT.get("da"));

                        objectMap.put("ret_flight", x.getFlight_id());

                        Intent o = new Intent(ResultFlightFragment.this, WebTiket.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        o.putExtra("status", "0");
                        startActivity(o);
                        flights.clear();
                        finish();
                        STATE = 0;
                        setmStopHandler();
                    }
                } else {

                    Intent o = new Intent(ResultFlightFragment.this, WebTiket.class);
                    o.putExtra("status", "1");
                    o.putExtra("flight", x.getFlight_id());
                    o.putExtra("date", TIKETCOM_API.DATA_FLIGHT.get("date"));
                    startActivity(o);
                    STATE = 0;
                }
            }
        });
    }

    class BG_RESULT_FLIGHT extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
                    asyncHttpClient.get(ResultFlightFragment.this, TIKETCOM_API.GET_FLIGHT, TIKETCOM_API.REQUEST_PARAM, new AsyncHttpResponseHandler() {

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            super.onFailure(statusCode, headers, responseBody, error);
//                            Log.d("json response", content);
                            error.printStackTrace();
                            final AlertDialog.Builder builder = new AlertDialog.Builder(ResultFlightFragment.this);
                            builder.setMessage("Jaringan terputus")
                                    .setPositiveButton("Kembali", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            setmStopHandler();
                                            startActivity(new Intent(ResultFlightFragment.this, MainActivity.class));
                                            finish();
                                            STATE = 0;
                                        }
                                    });
                            builder.setCancelable(false);
                            builder.show();
                        }

                        @Override
                        public void onSuccess(int statusCode, String content) {
                            Log.d("json response", content);
                            progres_flight.setVisibility(View.GONE);
                            try {
                                JSONObject response = new JSONObject(content);

                                if (response.getJSONObject("diagnostic").has("error_msgs")) {
                                    String error = response.getJSONObject("diagnostic").getString("error_msgs");
                                    final AlertDialog.Builder builder = new AlertDialog.Builder(ResultFlightFragment.this);
                                    builder.setMessage(error.replace("you are using insecure protocol,", ""))
                                            .setPositiveButton("Kembali", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    setmStopHandler();
                                                    finish();
                                                    STATE = 0;
                                                }
                                            });
                                    builder.setCancelable(false);
                                    if (!ResultFlightFragment.this.isFinishing()){
                                        builder.show();
                                    }
                                } else {

                                    JSONArray result = response.getJSONObject("departures").getJSONArray("result");
                                    if (result.length() > 0) {
                                        for (int i = 0; i < result.length(); i++) {
                                            JSONObject jsonObject = result.getJSONObject(i);
                                            Flight flight = new Flight();
                                            flight.setFlight_id(jsonObject.getString("flight_id"));
                                            Log.d("Flaght id",jsonObject.getString("flight_id"));

                                            flight.setAirlines_name(jsonObject.getString("airlines_name"));
                                            flight.setDuration(jsonObject.getString("duration"));
                                            flight.setPrice_value(jsonObject.getString("price_value"));
                                            flight.setFlight_number(jsonObject.getString("flight_number"));
                                            flight.setImage_url(jsonObject.getString("image"));
                                            flight.setFull_via(jsonObject.getString("full_via"));
                                            flight.setStop_flight(jsonObject.getString("stop"));

                                            adapterFlightSearch.clear();
                                            adapterFlightSearch.notifyDataSetChanged();

                                            adapterFlightSearch.add(flight);
                                            adapterFlightSearch.notifyDataSetChanged();
                                        }
                                        Log.d("STATE ", STATE + "");
                                        if (TIKETCOM_API.DATA_FLIGHT.get("ret_date").trim().length() > 0 && STATE == 3) {
                                            JSONArray returns = response.getJSONObject("returns").getJSONArray("result");
                                            for (int i = 0; i < returns.length(); i++) {
                                                JSONObject jsonObject = returns.getJSONObject(i);
                                                Flight flight = new Flight();
                                                flight.setAirlines_name(jsonObject.getString("airlines_name"));
                                                flight.setDuration(jsonObject.getString("duration"));
                                                flight.setFlight_id(jsonObject.getString("flight_id"));
                                                flight.setPrice_value(jsonObject.getString("price_value"));
                                                flight.setFlight_number(jsonObject.getString("flight_number"));
                                                flight.setImage_url(jsonObject.getString("image"));
                                                flight.setFull_via(jsonObject.getString("full_via"));
                                                flight.setStop_flight(jsonObject.getString("stop"));
//                                                flights.add(flight);
                                                adapterFlightSearch.clear();
                                                adapterFlightSearch.notifyDataSetChanged();

                                                adapterFlightSearch.add(flight);
                                                adapterFlightSearch.notifyDataSetChanged();
                                            }
                                        } else {
                                            if (STATE == 3) {
                                                final AlertDialog.Builder builder = new AlertDialog.Builder(ResultFlightFragment.this);
                                                builder.setMessage("Belum ada penerbangan Pulang")
                                                        .setPositiveButton("Kembali", new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {

                                                                setmStopHandler();
                                                                finish();
                                                                STATE = 0;
                                                            }
                                                        });
                                                builder.setCancelable(false);
                                                if (!ResultFlightFragment.this.isFinishing()){
                                                    builder.show();
                                                }
                                            }
                                        }

                                    } else {
                                        final AlertDialog.Builder builder = new AlertDialog.Builder(ResultFlightFragment.this);
                                        builder.setMessage("Penerbangan belum ditemukan, Pilih tanggal atau rute berbeda")
                                                .setPositiveButton("Kembali", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {

                                                        setmStopHandler();
                                                        finish();
                                                        STATE = 0;
                                                    }
                                                });
                                        builder.setCancelable(false);

                                        if (!ResultFlightFragment.this.isFinishing()){
                                            builder.show();
                                        }
                                    }
                                }

                            } catch (JSONException e) {
                                Log.d("json response", e.getMessage());
                                setmStopHandler();
                                e.printStackTrace();
                            }

                            Log.d("STATE ", STATE + "");
                            if (STATE == 0) {
                                if (TIKETCOM_API.DATA_FLIGHT.get("ret_date").trim().length() > 0) {
                                    STATE = 2;
                                } else {
                                    STATE = 1;
                                }
                            }

                        }
                    });
                }
            });
            return null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (STATE == 2) {
            Intent o = new Intent(ResultFlightFragment.this, ResultFlightFragment.class);
            startActivity(o);
            finish();
            STATE = 1;
        } else {
            STATE = 0;
            finish();
        }

        setmStopHandler();
    }

    public void setmStopHandler(){
        mStopHandler = false;
        mHandler.removeCallbacks(runnable);
    }
}
