package com.dodulz.papaantarin.papaantar.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.SearchActivity;
import com.dodulz.papaantarin.papaantar.bean.Armada;
import com.dodulz.papaantarin.papaantar.bean.ArmadaRepo;
import com.dodulz.papaantarin.papaantar.bean.Contact;
import com.dodulz.papaantarin.papaantar.bean.ContactRepo;
import com.dodulz.papaantarin.papaantar.bean.Travel;
import com.dodulz.papaantarin.papaantar.bean.TravelRepo;
import com.dodulz.papaantarin.papaantar.util.AsyncSAVETravel;
import com.dodulz.papaantarin.papaantar.util.ServiceHandler;
import com.dodulz.papaantarin.papaantar.util.StaticVariable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by dodulz on 3/7/14 AD.
 */
public class LoaderFragment extends Fragment {

    String berangkat = null;
    String tiba = null;
    String vendor = null;
    ServiceHandler serviceHandler1 = new ServiceHandler();
    TextView loadertext;
    ProgressBar pg_loader;
    public LoaderFragment(String berangkat,String tiba,String vendor){
        this.berangkat = berangkat;
        this.tiba = tiba;
        this.vendor = vendor;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SearchActivity.FROM_SEARCH_FRAGMENT = 0;
        View view = inflater.inflate(R.layout.loader_fragment,container,false);
        hiddenKeyboard(view);
        loadertext = (TextView)view.findViewById(R.id.datamemuat);
        pg_loader = (ProgressBar) view.findViewById(R.id.pg_load);

        new bg_process().execute();
        return view;
    }

    private void hiddenKeyboard(View v) {
        InputMethodManager keyboard = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        keyboard.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    class bg_process extends AsyncTask<Void,Void,Boolean> {

        Travel travel ;
        TravelRepo travelRepo ;

        public bg_process(){
            travel = new Travel();
            travelRepo = new TravelRepo(getActivity());

        }
        public boolean process() throws IOException {
            String URLi = "";
            String query_uri = "";
            try {
                query_uri = URLEncoder.encode(berangkat,"utf-8")+"/"+URLEncoder.encode(tiba,"utf-8")+"/"+URLEncoder.encode(vendor,"utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            URLi = StaticVariable.ROUTE+ query_uri;
//            String response = serviceHandler1.makeServiceCall(URLi,ServiceHandler.GET);
            String response = serviceHandler1.HTTP_GET(URLi);
            if (response.equals("{\"status\":\"no data\"}")){
                return false;
            }else{
                Log.i("Respon Informatin",response);
                travelRepo.delete_all(1);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    Log.i("total travel",jsonArray.length()+"");
                    JSONArray contactArray[] = new JSONArray[jsonArray.length()];
                    JSONArray armadaArray[] = new JSONArray[jsonArray.length()];
                    String slugArray[] =  new String[jsonArray.length()];

                    for (int i =0;i < jsonArray.length();i++){
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        JSONArray jsonArray1 = jsonObject1.getJSONArray("contacts");
                        Log.i("total contact travel "+i,jsonArray1.length()+"");

                        JSONArray jsonArray2 = jsonObject1.getJSONArray("rutes");
                        Log.i("total rutes travel "+i,jsonArray2.length()+"");


                        travel.setName(jsonObject1.getString("name"));
                        travel.setSlug_name(jsonObject1.getString("slug"));
                        travel.setCity(jsonObject1.getString("city"));
                        travel.setHarga(jsonArray2.getJSONObject(0).getString("price"));
                        travel.setType(jsonObject1.getString("type"));
                        travel.setAddress(jsonObject1.getString("address"));
                        travel.setCountry(jsonObject1.getString("country"));
                        travel.setFacilities(jsonObject1.getString("facilities"));
                        travel.setState(jsonObject1.getString("state"));
                        travel.setStatus_query(1);
                        travelRepo.create(travel);

                        contactArray[i] = jsonArray1;
                        armadaArray[i] = jsonObject1.getJSONArray("armadas");
                        slugArray[i] = jsonObject1.getString("slug");
                    }
                    new AsyncSAVETravel(getActivity(),contactArray,armadaArray,slugArray).execute();
                    return true;
                } catch (JSONException e) {
                    Log.e("JSON ERROR ",e.getMessage());
                    return false;
                }
            }
        }
        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                return process();
            }catch (IOException e){
                return false;
            }
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (aBoolean){
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new ResultSearchFragment(berangkat,tiba,vendor))
                        .commit();
            }else{
                pg_loader.setVisibility(View.GONE);
                loadertext.setText("Data belum tersedia...");
            }
        }
    }
}
