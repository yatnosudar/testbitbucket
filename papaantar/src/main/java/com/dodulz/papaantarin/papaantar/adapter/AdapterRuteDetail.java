package com.dodulz.papaantarin.papaantar.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dodulz.papaantarin.papaantar.R;
import com.dodulz.papaantarin.papaantar.bean.Rutes;

import java.util.List;

/**
 * Created by dodulz on 3/9/14 AD.
 */
public class AdapterRuteDetail extends ArrayAdapter<Rutes> {
    Activity context;
    int resource = 0;
    List<Rutes> objects;

    static class ViewHolder{
        public TextView txt_key;
        public TextView txt_value;
    }
    public AdapterRuteDetail(Activity context, int resource, List<Rutes> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rootView = null;
        if (rootView == null){
            LayoutInflater inflater = context.getLayoutInflater();
            rootView = inflater.inflate(resource,null);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.txt_key = (TextView) rootView.findViewById(R.id.id_key);
            viewHolder.txt_value = (TextView) rootView.findViewById(R.id.id_val);
            rootView.setTag(viewHolder);
        }else{
            rootView = convertView;

        }
        ViewHolder holder = (ViewHolder) rootView.getTag();
        holder.txt_key.setText(objects.get(position).getPlace());
        String harga = objects.get(position).getPrice();
        holder.txt_value.setText("Rp. "+harga.format("%,d", Integer.parseInt(harga)));
        return rootView;
    }

}
