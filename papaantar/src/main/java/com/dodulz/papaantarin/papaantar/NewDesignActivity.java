package com.dodulz.papaantarin.papaantar;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.dodulz.papaantarin.papaantar.about.Tentang;
import com.dodulz.papaantarin.papaantar.fragment.BookmarkFragment;
import com.dodulz.papaantarin.papaantar.fragment.PlaceFragment;
import com.dodulz.papaantarin.papaantar.fragment.SearchFragment;
import com.dodulz.papaantarin.papaantar.fragment.TiketFragment;
import com.dodulz.papaantarin.papaantar.tiketcom.TIKETCOM_API;
import com.dodulz.papaantarin.papaantar.tiketcom.WebTiket;
import com.dodulz.papaantarin.papaantar.util.StaticVariable;

/**
 * Created by dodulz on 3/26/14.
 */
public class NewDesignActivity extends ActionBarActivity {


    Button vendor,search,favorit;
    ImageButton more;

    int actionbar_menu = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(0, 0);
        setContentView(R.layout.main_layout);

        init();

        Intent i = getIntent();
        int ix = i.getIntExtra("status",1);


//        if (savedInstanceState == null) {
        if (ix==1){
            reset_menu();
            actionbar_menu = 0;
            vendor.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.vendor_active), null, null);
            vendor.setTextColor(getResources().getColor(R.color.orange));
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_main, new PlaceFragment())
                    .commit();
            setTitle("Vendor");
        }
        else if(ix==2){
            reset_menu();
            actionbar_menu = 1;
            search.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.search_active), null, null);
            search.setTextColor(getResources().getColor(R.color.orange));
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_main, new SearchFragment())
                    .commit();
            setTitle("Cari Travel");
        }
        else if(ix==3){
            reset_menu();
            actionbar_menu = 2;
            favorit.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.bookmark_active), null, null);
            favorit.setTextColor(getResources().getColor(R.color.orange));
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_main, new BookmarkFragment())
                    .commit();
            setTitle("Favorit");
        }
        else if(ix==4){
            reset_menu();
            actionbar_menu = 3;
            more.setImageDrawable(getResources().getDrawable(R.drawable.more_active));
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_main, new TiketFragment())
                    .commit();
            setTitle("Tiket");
        }
    }

    public void init(){
        vendor = (Button) findViewById(R.id.image_vendor);
        search = (Button) findViewById(R.id.image_search);
        favorit = (Button) findViewById(R.id.image_bookmark);
        more = (ImageButton) findViewById(R.id.more);


    }

    private void hiddenKeyboard(View v) {
        InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        keyboard.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public void reset_menu(){
        vendor.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.vendor), null, null);
        vendor.setTextColor(getResources().getColor(R.color.silver));

        search.setCompoundDrawablesWithIntrinsicBounds(null,getResources().getDrawable(R.drawable.search),null,null);
        search.setTextColor(getResources().getColor(R.color.silver));

        favorit.setCompoundDrawablesWithIntrinsicBounds(null,getResources().getDrawable(R.drawable.bookmark),null,null);
        favorit.setTextColor(getResources().getColor(R.color.silver));

        more.setImageDrawable(getResources().getDrawable(R.drawable.more));

    }
    public void _vendor(View v){

        reset_menu();
        actionbar_menu = 0;
        vendor.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.vendor_active), null, null);
        vendor.setTextColor(getResources().getColor(R.color.orange));
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container_main, new PlaceFragment())
                .commit();
        setTitle("Vendor");
        hiddenKeyboard(v);
    }

    public void _cari(View v){
        reset_menu();
        actionbar_menu = 1;
        search.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.search_active), null, null);
        search.setTextColor(getResources().getColor(R.color.orange));
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container_main, new SearchFragment())
                .commit();
        setTitle("Cari Travel");
    }

    public void _bookmark(View v){
        reset_menu();
        actionbar_menu = 2;
        favorit.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.bookmark_active), null, null);
        favorit.setTextColor(getResources().getColor(R.color.orange));
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container_main, new BookmarkFragment())
                .commit();
        setTitle("Favorit");
        hiddenKeyboard(v);
    }

    public void _more(View v){
//        reset_menu();
        actionbar_menu = 3;
//        more.setImageDrawable(getResources().getDrawable(R.drawable.more_active));
//        getSupportFragmentManager().beginTransaction()
//                .replace(R.id.container_main, new TiketFragment())
//                .commit();
//        setTitle("Tiket");

        final Dialog dialog = new Dialog(NewDesignActivity.this,R.style.DialogAnimation);
        dialog.getWindow().setWindowAnimations(R.style.DialogAnimation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_more);
        Button btn1 = (Button) dialog.findViewById(R.id.btn1);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (StaticVariable.isOnline()){
                    if (TIKETCOM_API.TOKEN.length()==0){
                        TIKETCOM_API.token();
                    }
                    startActivity(new Intent(NewDesignActivity.this, WebTiket.class).putExtra("status", "10"));

                    //getActivity().startActivity(new Intent(getActivity(), TiketActivity.class).putExtra("spot","0"));
                }else{
                    Toast.makeText(NewDesignActivity.this, "Tidak ada jaringan...", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });
        Button btn2 = (Button) dialog.findViewById(R.id.btn2);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (StaticVariable.isOnline()){

                    startActivity(new Intent(NewDesignActivity.this, WebTiket.class).putExtra("status", "3"));
                }else{
                    Toast.makeText(NewDesignActivity.this,"Tidak ada jaringan...",Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });
        Button btn3 = (Button) dialog.findViewById(R.id.btn3);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (StaticVariable.isOnline()){
                    startActivity(new Intent(NewDesignActivity.this, WebTiket.class).putExtra("status", "2"));
                }else{
                    Toast.makeText(NewDesignActivity.this,"Tidak ada jaringan...",Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });
        Button btn4 = (Button) dialog.findViewById(R.id.btn4);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(NewDesignActivity.this,VendorAgent.class));
                dialog.dismiss();
            }
        });
        Button btn5 = (Button) dialog.findViewById(R.id.btn5);
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(NewDesignActivity.this,Tentang.class));
                dialog.dismiss();
            }
        });
        dialog.show();
        hiddenKeyboard(v);
    }


}
